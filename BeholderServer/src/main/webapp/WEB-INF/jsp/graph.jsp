<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<head>
<script src="${context}/js/when.js"></script>
<script src="${context}/js/vivagraph.js"></script>
<script src="${context}/js/graphUI.js"></script>
<script src="${context}/js/graph.js"></script>
<script src="${context}/js/jquery-1.11.0.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<script src="${context}/js/bootstrap-switch.min.js"></script>
<link rel="stylesheet" type="text/css" href="${context}/css/main.css">
<link rel="stylesheet" type="text/css" href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap-switch.min.css">
<title>Beholder</title>
</head>
<body>
	<div id="container"></div>
	<div id="leftbox" class="graph_outer_box">
		<span class="glyphicon glyphicon-collapse-up" id="collapse_button"
			data-toggle="collapse" data-target="#option"></span>
		<div class="graph_inner_box collapse in" id="option">
			<input class="btn-primary" type="button"
				onclick="(function(){window.location.href=document.referrer})()"
				value="Back" /> <label>Drag</label> <input type="checkbox"
				id="mode-switch" name="mode" checked="checked" /> <label>Show
				Label</label> <input type="checkbox" id="label-switch" name="label"
				checked="checked" /> <label>Show Edge</label> <input
				type="checkbox" id="edges-switch" name="edges" checked="checked" />
			<label>Metrics</label>
			<div class="btn-group " id="width">
				<label>Width</label>
				<button type="button"
					class="btn btn-default dropdown-toggle dropdown_button"
					name="width" data-toggle="dropdown">
					Default<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li class="dropdown_option" onclick="GraphUI.setWidth()">Default</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setWidth('il')">Internal
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setWidth('el')">External
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setWidth('fl')">Faulty
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setWidth('bl')">Files</li>
				</ul>
			</div>
			</br>
			<div class="btn-group " id="height">
				<label>Height</label>
				<button type="button"
					class="btn btn-default dropdown-toggle dropdown_button"
					name="height" data-toggle="dropdown">
					Default<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li class="dropdown_option" onclick="GraphUI.setHeight()">Default</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setHeight('il')">Internal
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setHeight('el')">External
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setHeight('fl')">Faulty
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setHeight('bl')">Files</li>
				</ul>
			</div>
			</br>
			<div class="btn-group " id="color">
				<label>Color</label>
				<button type="button"
					class="btn btn-default dropdown-toggle dropdown_button"
					name="color" data-toggle="dropdown">
					Default<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" role="menu">
					<li class="dropdown_option" onclick="GraphUI.setColor()">Default</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setColor('il')">Internal
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setColor('el')">External
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setColor('fl')">Faulty
						Links</li>
					<li class="divider"></li>
					<li class="dropdown_option" onclick="GraphUI.setColor('bl')">Files</li>
				</ul>
			</div>
		</div>
	</div>

	<div id="rightbox" class="graph_outer_box">
		<button type="button" id="close_button" class="close"
			onclick="GraphUI.emptyBox()">&times</button>
		<div class="graph_inner_box" id="info"></div>
	</div>
	<div id="status_bar">
		<div id="inner_status_bar"></div>
	</div>

	<div class="dropdown clearfix" id="context_menu">
		<ul style="display: block; position: static; margin-bottom: 5px;"
			class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			<li class="dropdown_option" onclick="GraphUI.showFaultyModal()">Faulty Link List</li>
			<li role="presentation" class="divider"></li>
			<li class="dropdown_option" onclick="GraphUI.showFilesModal()">File List</li>
			<li role="presentation" class="divider"></li>
			<li class="dropdown_option" onclick="GraphUI.killNode()">Kill
				Node</li>
		</ul>
	</div>

	<div class="dropdown clearfix" id="group_context_menu">
		<ul style="display: block; position: static; margin-bottom: 5px;"
			class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			<li class="dropdown_option" onclick="GraphUI.killSelectedNodes()">Kill
				Nodes</li>
		</ul>
	</div>

	<div class="modal fade" id="graphModal" tabindex="-1" role="dialog"
		aria-labelledby="graphModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="graphModalLabel"></h4>
					<h5 class="modal-title" id="graphModalSubLabel"></h5>
				</div>
				<div class="modal-body" id="graphModalBody"></div>
			</div>
		</div>
	</div>

</body>
<script>
	MyGraph.getGraph("${param.id}");
	GraphUI.init();
</script>