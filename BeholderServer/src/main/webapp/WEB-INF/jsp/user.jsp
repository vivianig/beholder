<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<head>
<script src="${context}/js/jquery-1.11.0.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<script src="${context}/js/bootstrap-switch.min.js"></script>
<script src="${context}/js/user.js"></script>
<script src="${context}/js/when.js"></script>
<link rel="stylesheet" type="text/css" href="${context}/css/main.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap-switch.min.css">
</head>
<body>

	<header>
		<h1>Beholder</h1>
	</header>
	<div id="crawler">
		<div id="crawler_inner" class="user_inner_box">
			<h3>Crawler</h3>
			<form action="crawl" id="crawler_form" method="post">
				<input class="form-control text_input" type="text" name="url"
					placeholder="Insert URL" /> <input
					class="btn-primary button_input" type="submit" value="Crawl" />
			</form>
			<div id="crawler_counter">
				<div id='counter'></div>
				<button class="btn-primary button_input" onclick="cancelJob()">Cancel Job</button>
			</div>
			<div id="alerts"></div>
		</div>
	</div>
	<div id="graphs">
		<div id="graphs_inner">
			<h3>Graphs</h3>
			<div id="graphs_list">
				<c:forEach items="${requestScope.graphs}" var="graph">
					<div class="graph_box" name="${graph.id}">
						<button type='button' class='close'>&times;</button>
						<table>
							<tbody>
								<tr>
									<td><label>Url</label> ${graph.url}</td>
									<td><label>Date</label> ${graph.date}</td>
								</tr>
								<tr>
									<td><label>Internal Links</label> ${graph.internalLinks}</td>
									<td><label>External Links</label> ${graph.externalLinks}</td>
								</tr>
								<tr>
									<td><label>Files</label> ${graph.files}</td>
									<td><label>Faulty Links</label> ${graph.faultyLinks}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</c:forEach>

			</div>
		</div>
	</div>

</body>
<script>
	$(".alert").alert();
	$("body").on("click", "div.graph_box", function() {
		window.location = "${context}/graph?id=" + $(this).attr("name");
	});
	$("body").on("click", ".close", function(e) {
		e.preventDefault();
		e.stopPropagation();
		removeGraph($(this).parent());
		return false;
	});
	window.checkInterval = setInterval(function() {
		checkExistingJob();
	}, 10000);
</script>

