<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="context" value="${pageContext.request.contextPath}" />
<head>
<script src="${context}/js/jquery-1.11.0.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<script src="${context}/js/bootstrap-switch.min.js"></script>
<script src="${context}/js/bootstrap-validator.min.js"></script>
<script src="${context}/js/index.js"></script>

<link rel="stylesheet" type="text/css" href="${context}/css/main.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap-switch.min.css">
<link rel="stylesheet" type="text/css"
	href="${context}/css/bootstrap-validator.min.css">
<title>Beholder</title>
</head>
<body>
	<header>
		<h1>Beholder</h1>
	</header>
	<div id="loginBackground">
		<div id="login_box">
			<c:if test="${not empty sessionScope.error}">
				<p id="error">${sessionScope.error}</p>
				<c:remove var="error" scope="session" />
			</c:if>
			<form action="login" method="post" id="login">
				<div class="form-group">
					<input class="form-control text_input" type="text" name="username"
						placeholder="Username" />
				</div>
				<div class="form-group">
					<input class="form-control text_input" type="password"
						name="password" placeholder="Password" />
				</div>
				<!--  <div class="check_box">
					<input name="remember" type="checkbox" id="remember"
						value="forever" /> &nbsp;Remember me&nbsp;&nbsp;
				</div>-->
				<div class="form-group">
					<input id="login-button" class="btn-primary button_input"
						type="submit" value="Login" />
				</div>
			</form>
			<button id="register-button" class="btn-primary button_input"
				data-toggle="modal" data-target="#register-modal">Register
			</button>

		</div>
	</div>
	<div id="register-modal" class="modal fade">
		<div class="modal-dialog in" aria-hidden="false">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">�</button>
					<h4 class="modal-title">Registration</h4>
				</div>
				<div class="modal-body">
					<form action="register" method="post" id="register">
						<div class="form-group">
							<input class="form-control text_input" type="text"
								name="username" placeholder="Username" />
						</div>
						<div class="form-group">
							<input class="form-control text_input" type="password"
								name="password" placeholder="Password" />
						</div>
						<div class="form-group">
							<input id="login-button" class="btn-primary button_input"
								type="submit" value="Register" />
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>