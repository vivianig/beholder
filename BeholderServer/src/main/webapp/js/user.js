window.currentUrl = "";
$("document").ready(function(e) {
	$("#crawler_form").submit(function(e) {
		var xmlHttp = new XMLHttpRequest();
		xmlHttp.open( "POST", "crawl", false);
		xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		xmlHttp.onreadystatechange = function() {
			if (xmlHttp.readyState==4 && xmlHttp.status==200) {
				var result = jQuery.parseJSON(xmlHttp.responseText);
				if(result) {
					switch(result.status) {
					case 0:
						checkExistingJob();
						break;
					case 1:
						$("#alerts").prepend(result.html);
						break;
					case 2:
						$("#alerts").prepend(result.html);
					default:
						break;
					}
				}
			}
		};
		xmlHttp.send("url="+$( "input:first" ).val());
		$("#crawler_form")[0].reset();
		return false;
	});
});

function getCookieUser() {
	var name = "beholder=";
	var ca = document.cookie.split(';');
	for(var i=0; i<ca.length; i++) {
		var c = ca[i].trim();
		if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
	}
	return "";
};


function removeGraph(box) {
	var id = box.attr("name")
	box[0].remove();
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4 && xmlHttp.status==200) {
		}     
	};
	xmlHttp.open( "POST", "removeGraph?id="+id, false);
	xmlHttp.send( undefined );
}

function startCounter(url) {
	window.currentUrl = url;
	window.updateInterval = setInterval(function() {requestUpdate(url);}, 1000);
}

function checkExistingJob() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4 && xmlHttp.status==200) {
			var result = jQuery.parseJSON(xmlHttp.responseText);
			if(result) {
				switch(result.status) {
				case 0:
					break;
				case 1:
					clearInterval(window.checkInterval);
					startCounter(result.url);
					break;
				default:
					break;
				}
			}
		}     
	};
	xmlHttp.open( "GET", "crawlerCurrentStatus", false);
	xmlHttp.send( undefined );	
}

function requestUpdate(url) {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4 && xmlHttp.status==200) {
			var result = jQuery.parseJSON(xmlHttp.responseText);
			if(result) {
				switch(result.status) {
				case 0:
					$("#crawler_counter").show();
					$("#counter").html(result.html);
					break;
				case 1:
					window.currentUrl ="";
					if(result.user === getCookieUser()) 
						$('#graphs_list').append(result.html);
					$("#crawler_counter").hide();
					clearInterval(window.updateInterval);
					window.checkInterval = setInterval(function() {checkExistingJob();}, 10000);
					break;
				case 2:
					window.currentUrl ="";
					if(result.user === getCookieUser()) 
						$("#alerts").prepend(result.html);
					$("#crawler_counter").hide();
					clearInterval(window.updateInterval);
					window.checkInterval = setInterval(function() {checkExistingJob();}, 10000);
					break;
				default:
					break;
				}
			}
		}     
	};
	xmlHttp.open( "GET", "crawlerStatus?url="+url, false);
	xmlHttp.send( undefined );	
}

function cancelJob() {
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4 && xmlHttp.status==200) {
			var result = jQuery.parseJSON(xmlHttp.responseText);
			if(result) {
				switch(result.status) {
				case 0:
					clearInterval(window.updateInterval);
					checkExistingJob();
					break;
				case 1:
					$("#alerts").prepend(result.html);
					break;
				default:
					break;
				}
			}
		}     
	};
	xmlHttp.open( "POST", "cancelJob?url="+window.currentUrl, false);
	xmlHttp.send( undefined );	
}