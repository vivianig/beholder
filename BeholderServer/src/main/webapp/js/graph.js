var MyGraph = {};
MyGraph.url = undefined;
MyGraph.selectedNodes = [];

MyGraph.addClass = function(element, className) {
	element.setAttribute('class', element.getAttribute('class') + " " + className);
};

MyGraph.removeClass = function(element, className) {
	if(element.getAttribute('class') === null) {
		element.setAttribute('class', '');
	}
	re = new RegExp(" " +className, 'g');
	element.setAttribute('class', element.getAttribute('class').replace(re, ''));
};

MyGraph.scaleEdgesStrength = function(edges) {
	var min = 1,	//MIN
	max = 3,		//MAX
	edgesMin,
	edgesMax;
	edges.forEach(function(edge) {
		if(edgesMin === undefined || edge.value < edgesMin ) {
			edgesMin = edge.value;
		}
		if(edgesMax === undefined || edge.value > edgesMax )
			edgesMax = edge.value;
	});
	edges.forEach(function(edge) {
		if(edgesMin !== edgesMax)
			edge.value = ((max - min) * (edge.value - edgesMin) / (edgesMax - edgesMin)) + min;
	});	
};

MyGraph.getGraph = function(id) {
	MyGraph.requestJSON("getGraph?id="+id).then(function(json) {
		MyGraph.generateGraph(json);
	});
};

MyGraph.requestJSON = function(url) {
	var deferred = when.defer();
	var xmlHttp = new XMLHttpRequest();
	xmlHttp.onreadystatechange = function() {
		if (xmlHttp.readyState==4 && xmlHttp.status==200) {
			var json = jQuery.parseJSON(xmlHttp.responseText);
			deferred.resolve(json);
		} else if(xmlHttp.readyState==4) {
			deferred.reject(new Error("Error while getting: " + url));
		}           
	};
	xmlHttp.open( "GET", url, true);
	xmlHttp.send( undefined );
	return deferred.promise;
};

MyGraph.binaryNodes = function(node) {
	if(node.showBinaryNodes === undefined)
		return;
	MyGraph.requestJSON("binaryNodes?id="+node.node.id).then(function(json) {
		MyGraph.graph.beginUpdate();
		var x = json.length,
		i=0;
		json.forEach(function(newNode) {
			if(MyGraph.graph.getNode(newNode.id) !== undefined) {
				x--;
			}
		});
		var distance = Math.max(60*Math.log(x),60);
		json.forEach(function(newNode) {
			if(node.showBinaryNodes) {
				if(MyGraph.graph.getNode(newNode.id).links.length > 1) 
					MyGraph.graph.forEachLinkedNode(newNode.id, function(linkedNode, link) {
						if(link.fromId === node.id)
							MyGraph.graph.removeLink(link);
					});
				else 
					MyGraph.graph.removeNode(newNode.id);
			} else {
				if(MyGraph.graph.getNode(newNode.id) === undefined) {
					var graphNode = MyGraph.graph.addNode(newNode.id);             
					graphNode.x = distance*Math.cos(i/x*2* Math.PI)+node.x;
					graphNode.y = distance*Math.sin(i/x*2* Math.PI)+node.y;
					graphNode.color ="#FF0000";
					graphNode.node = newNode;
					i++;
				}
				var link = MyGraph.graph.addLink(node.id, newNode.id);
				link.color = "#F00";
				link.strength = 1;
			}
		}); 
		node.showBinaryNodes = !node.showBinaryNodes;
		MyGraph.graph.endUpdate();
	});
};

MyGraph.highLightRelatedNodes = function(node,on) {
	MyGraph.graph.forEachLink(function(link) {
		var linkUI = MyGraph.graphics.getLinkUI(link.id);
		if (linkUI)
			linkUI.attr('stroke', 'gray');
	});
	MyGraph.graph.forEachNode(function(node) {
		var nodeUI = MyGraph.graphics.getNodeUI(node.id);
		if(nodeUI) {
			var rect = nodeUI.childNodes[0];
			MyGraph.removeClass(rect, "highlighted");
		};
	});
	if(on) {
		MyGraph.graph.forEachLinkedNode(node.id, function(node, link) {
			var nodeUI = MyGraph.graphics.getNodeUI(node.id);
			if(nodeUI) {
				var rect = nodeUI.childNodes[0];
				MyGraph.addClass(rect, "highlighted");
			}
			var linkUI = MyGraph.graphics.getLinkUI(link.id);
			if(linkUI)
				linkUI.attr('stroke', 'red');
		});
	}; 
};

MyGraph.generateGraph = function(graph) {
	console.log(graph);
	MyGraph.url = graph.url;
	MyGraph.graph = Viva.Graph.graph();

	var geom = Viva.Graph.geom();

	graph.nodes.forEach(function(node) {
		graphNode = MyGraph.graph.addNode(node.id);
		graphNode.node = node;
		graphNode.x = node.x;
		graphNode.y = node.y;
		graphNode.color = "white";
		graphNode.showBinaryNodes = false;
	});
	
	MyGraph.scaleEdgesStrength(graph.edges);
	
	graph.edges.forEach(function(edge) {
		if(MyGraph.graph.getNode(edge.source) !== undefined && MyGraph.graph.getNode(edge.target)!== undefined) {
			var link = MyGraph.graph.addLink(edge.source, edge.target);
			link.strength = edge.value;
		}
	});

	MyGraph.graphics = Viva.Graph.View.svgGraphics(),
	layout = Viva.Graph.Layout.constant(MyGraph.graph);
	layout.placeNode(function(node) {
		return({ x : node.x, y : node.y});
	});
	MyGraph.renderer = Viva.Graph.View.renderer(MyGraph.graph, {
		container  : document.getElementById('container'),
		graphics : MyGraph.graphics,
		layout : layout
	});

	MyGraph.graphics.node(function(node) {
		var ui =  Viva.Graph.svg("g")
		.attr("width", 10)
		.attr("height", 10),
		rect = Viva.Graph.svg("rect")
		.attr("width", 10)
		.attr("height", 10)
		.attr("stroke-width", 2)
		.attr("stroke", "rgb(0,0,0)")
		.attr("fill", node.color)
		.attr("class", "node");
		ui.append(rect);

		$(ui).hover(function() {
			MyGraph.highLightRelatedNodes(node, true);
			if(!GraphUI.labelsToggled)
				GraphUI.toggleLabel(node, true);
			GraphUI.fillBar(node);
		}, function () {
			MyGraph.highLightRelatedNodes(node, false);
			if(!GraphUI.labelsToggled)
				GraphUI.toggleLabel(node, false);
		});

		$(ui).dblclick(function() {
			MyGraph.binaryNodes(node);
		});

		$(ui).click(function() {
			GraphUI.fillBox(node);
		});

		$(ui).on("contextmenu", function(e) {
			e.preventDefault();
			e.stopPropagation();
			var menu = $("#context_menu");
			menu.show();
			menu.offset({ top: e.pageY, left: e.pageX});
			menu.data("node", node);
		});

		return ui;
	}).placeNode(function(nodeUI, pos, node) {
		nodeUI.attr('transform', 
				'translate(' + (node.x - nodeUI.getAttribute("width")/2)
				+ ',' + (node.y - nodeUI.getAttribute("height")/2) + ')');
		if(nodeUI.childElementCount > 1) {
			var svg = $("#container > svg")[0], 
			transform = svg.firstChild.getAttribute("transform").slice(7, -1).replace(/\s/g, '').split(","),
			scale = parseFloat(transform[0]);
			nodeUI.children[1].attr('transform', 'scale('+(1/scale)+')').children[0].attr('y', (-2*scale)+'px');
		}
		
	}).link(function(link) {
		var ui = Viva.Graph.svg("path")
		//.attr('marker-end', 'url(#Triangle)')
		.attr("class", "arrow-link")
		.attr("fill", "#666");
		//.attr("stroke", "#FFF")
		//.attr("stroke-width", 1);
		ui.link = link;
		ui.from = MyGraph.graphics.getNodeUI(link.toId);
		ui.to = MyGraph.graphics.getNodeUI(link.fromId);
		
		return ui;
	}).placeLink(function(linkUI, fromPos, toPos) {
		var toWidth = linkUI.from.getAttribute("width"),
		toHeight = linkUI.from.getAttribute("height"),
		fromWidth = linkUI.to.getAttribute("width"),
		fromHeight = linkUI.to.getAttribute("height");
		var from = geom.intersectRect(
				fromPos.x - (fromWidth / 2), // left
				fromPos.y - (fromHeight / 2), // top
				fromPos.x + (fromWidth / 2), // right
				fromPos.y + (fromHeight / 2), // bottom
				fromPos.x, fromPos.y, toPos.x, toPos.y)
				|| fromPos;

		var to = geom.intersectRect(
				toPos.x - (toWidth / 2), // left
				toPos.y - (toHeight / 2), // top
				toPos.x + (toWidth / 2), // right
				toPos.y + (toHeight / 2), // bottom
				toPos.x, toPos.y, fromPos.x, fromPos.y)
				|| toPos;

		
		var slope = Math.atan2(to.y - from.y, to.x - from.x);
		var strength = parseFloat(linkUI.link.strength);
		var radius = 5;
		
		var data = "M "
			+ (from.x + radius *  Math.cos(slope) - strength * Math.cos(slope + (Math.PI/2)))
			+ ", "
			+ (from.y +  radius *  Math.sin(slope) - strength * Math.sin(slope + (Math.PI/2)))
			
			+ " L " 
			+ (from.x +  radius *  Math.cos(slope))
			+ ", " 
			+ (from.y +  radius *  Math.sin(slope))
			
			+ " L "
			+ (to.x - radius *  Math.cos(slope))
			+ " , "
			+ (to.y - radius *  Math.sin(slope))
			
			+ " L " 
			+ (to.x - (radius + strength) * Math.cos(slope) - (2*strength)  * Math.cos(slope + (Math.PI/2)))
			+ " , "
			+ (to.y - (radius + strength) * Math.sin(slope) - (2*strength)  * Math.sin(slope + (Math.PI/2)))
			
			+ " L " 
			+ (to.x - (radius + strength) * Math.cos(slope) - (strength) * Math.cos(slope + (Math.PI/2)))
			+ " , "
			+ (to.y - (radius + strength) * Math.sin(slope) - (strength)  * Math.sin(slope + (Math.PI/2)))
			+ " Z";
		linkUI.attr("d", data);
	});
	MyGraph.renderer.run();
};
