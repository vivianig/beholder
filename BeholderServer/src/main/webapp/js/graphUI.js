var GraphUI = {};
GraphUI.labelsToggled = false;
GraphUI.widthMetric;
GraphUI.heightMetric;
GraphUI.colorMetric;
GraphUI.vivaHandlers;
GraphUI.currentMode = "drag";

GraphUI.init = function() {
	$("#mode-switch").bootstrapSwitch("state", true);
	$("#mode-switch").on("switchChange.bootstrapSwitch", function (e, data) {
		GraphUI.changeMode(data);
	});

	$("#label-switch").bootstrapSwitch("state", false);
	$("#label-switch").on("switchChange.bootstrapSwitch", function (e, data) {
		GraphUI.toggleLabels(data);
	});
	$("#edges-switch").bootstrapSwitch("state", true);
	$("#edges-switch").on("switchChange.bootstrapSwitch", function (e, data) {
		GraphUI.toggleEdges(data);
	});
	$(".collapse").on("shown.bs.collapse", function () {
		$("#collapse_button").removeClass("glyphicon-collapse-down").addClass("glyphicon-collapse-up");
	});
	$(".collapse").on("hidden.bs.collapse", function () {
		$("#collapse_button").removeClass("glyphicon-collapse-up").addClass("glyphicon-collapse-down");
	});
	$(document).ready(function(){
		$(document).bind("contextmenu",function(e){
			e.preventDefault();
			if(MyGraph.selectedNodes.length > 0) {
				$("#context_menu").hide();
				var menu = $("#group_context_menu");
				menu.show();
				menu.offset({ top: e.pageY, left: e.pageX});
			}
		});

		$("#container").mousedown(function(e) {
			var groupMenu = $("#group_context_menu");
			var menu = $("#context_menu")
			if(groupMenu.is(":visible") || menu.is(":visible")) {
				menu.hide();
				groupMenu.hide();
				return;
			}
			if(GraphUI.currentMode === "select" && e.button === 0) {
				MyGraph.graph.forEachNode(function(node) {
					var ui = MyGraph.graphics.getNodeUI(node.id);
					if(ui) {
						var rect = ui.childNodes[0];
						MyGraph.removeClass(rect, "selected");
					}
				});
				MyGraph.selectedNodes = [];

				var svgNS = 'http://www.w3.org/2000/svg',
				svg = $("#container > svg")[0];
				pt = svg.createSVGPoint();
				var point0 = getLocalCoordinatesFromMouseEvent(svg,e);
				var selector = document.createElementNS(svgNS,'rect');
				selector.setAttribute('class','selector');
				updateselector(selector,point0,point0);
				svg.appendChild(selector);
				document.documentElement.addEventListener('mousemove',trackMouseMove,false);
				document.documentElement.addEventListener('mouseup',stopTrackingMove,false);
			}

			function trackMouseMove(e){
				var point1 = getLocalCoordinatesFromMouseEvent(svg,e);
				updateselector(selector,point0,point1);
			}
			function stopTrackingMove(){
				var position = {x1 : selector.getAttribute("x"), y1: selector.getAttribute("y")},
				transform = svg.firstChild.getAttribute("transform").slice(7, -1).replace(/\s/g, '').split(","),
				scale = { x : parseFloat(transform[0]), y : parseFloat(transform[3])};

				//translation
				position.x1 -= parseFloat(transform[4]);
				position.y1 -= parseFloat(transform[5]);
				position.x2 = position.x1 + parseFloat(selector.getAttribute("width"));
				position.y2 = position.y1 + parseFloat(selector.getAttribute("height"));

				//scaling
				position.x1 /= scale.x;
				position.y1 /= scale.y;
				position.x2 /= scale.x;
				position.y2 /= scale.y;

				//set class
				selectNodes(position);

				document.documentElement.removeEventListener('mousemove',trackMouseMove,false);
				document.documentElement.removeEventListener('mouseup',stopTrackingMove,false);
				svg.removeChild(selector);
			}

			function callWithBBox(func,rect){
				var x = rect.getAttribute('x')*1,
				y = rect.getAttribute('y')*1,
				w = rect.getAttribute('width')*1,
				h = rect.getAttribute('height')*1;
				func(x,y,x+w,y+h);
			}

			function updateselector(rect,p0,p1){
				var xs = [p0.x,p1.x].sort(sortByNumber),
				ys = [p0.y,p1.y].sort(sortByNumber);
				rect.setAttribute('x',xs[0]);
				rect.setAttribute('y',ys[0]);
				rect.setAttribute('width', xs[1]-xs[0]);
				rect.setAttribute('height',ys[1]-ys[0]);
			}

			function selectNodes(position) {
				MyGraph.graph.forEachNode(function(node) {
					var ui = MyGraph.graphics.getNodeUI(node.id);
					if(ui) {
						var transformation = ui.getAttribute("transform").slice(10, -1).split(",");
						var coordinates = {x: transformation[0], y: transformation[1]};
						if(coordinates.x > position.x1 && coordinates.x < position.x2 && coordinates.y > position.y1 && coordinates.y < position.y2) {
							var rect = ui.childNodes[0];
							MyGraph.addClass(rect, "selected");
							MyGraph.selectedNodes.push(node);
						}
					}
				});
			}

			function getLocalCoordinatesFromMouseEvent(el,e){
				pt.x = e.clientX; pt.y = e.clientY;
				return pt.matrixTransform(el.getScreenCTM().inverse());
			}

			function sortByNumber(a,b){ return a-b; }
		});
	});
};

GraphUI.changeMode = function(toggle) {
	if(toggle) {
		GraphUI.currentMode = "drag";
	} else {
		GraphUI.currentMode = "select";
	}
};

GraphUI.toggleLabel = function(node, toggle) {
	var ui = MyGraph.graphics.getNodeUI(node.id);
	if(ui === undefined)
		return;
	var children = ui.childNodes;
	if(toggle) {
		if(children.length == 1) {
			var svg = $("#container > svg")[0], 
			transform = svg.firstChild.getAttribute("transform").slice(7, -1).replace(/\s/g, '').split(","),
			scale = parseFloat(transform[0]), 
			label = Viva.Graph.svg('g').attr('transform', 'scale('+(1/scale)+')'),
			text = Viva.Graph.svg('text').attr('y', (-2*scale)+'px').text(node.node.label);

			label.append(text);
			ui.append(label);
		}
	} else {
		if(children.length > 1)
			ui.lastChild.remove();
	}
};

GraphUI.toggleLabels = function(toggle) {
	GraphUI.labelsToggled = toggle;
	MyGraph.graph.forEachNode(function(node) {
		GraphUI.toggleLabel(node, toggle);
	});
};

GraphUI.toggleEdges = function(toggle) {
	MyGraph.graph.forEachLink(function(link) {
		var ui = MyGraph.graphics.getLinkUI(link.id);
		if(ui === undefined)
			return;
		//ui.attr('visibility', toggle ? 'visible' : 'hidden');
		ui.attr('display', toggle ? 'inline' : 'none');
	});
};

GraphUI.emptyBox = function() {
	$("#rightbox").hide();
};

GraphUI.fillBox = function(node) {
	$("#rightbox").show();
	var box = $("#info");

	var string = "<table><tbody>"
		+ "<tr><td class='graph_table_field'>Node:</td>"
		+ "<td class='graph_table_data'>" + node.node.label + "</td></tr>"
		+ "<tr><td class='graph_table_field'>Url:</td>"
		+ "<td class='graph_table_data'>" + node.node.url.substring(MyGraph.url.length-1) + "</td></tr>"
		if(node.node.type === "text/html") {
			string += "<tr><td class='graph_table_field'>Internal Links:</td>"
				+ "<td class='graph_table_data'>" + (node.node.internalLinks || 0) + "</td></tr>"
				+ "<tr><td class='graph_table_field'>External Links:</td>"
				+ "<td class='graph_table_data'>" + (node.node.externalLinks || 0) + "</td></tr>"
				+ "<tr><td class='graph_table_field'>Faulty Links:</td>"
				+ "<td class='graph_table_data'>" + (node.node.faultyLinks || 0) + "</td></tr>"
				+ "<tr><td class='graph_table_field'>Files:</td>"
				+ "<td class='graph_table_data'>" + (node.node.binaryNodes || 0) + "</td></tr>"
				+ "</tbody></table>";
		}
	box.html(string);
};

GraphUI.emptyBar = function() {
	$("#inner_status_bar").html("");
};

GraphUI.fillBar = function(node) {
	GraphUI.emptyBar();
	var string = ""
		string += "<p>" + node.node.label + " - " + node.node.url;
	//string += "</br>"
	if(node.node.type != "text/html") {
		string += "</p>";
		$("#inner_status_bar").append(string);
		return;
	}


	switch(GraphUI.widthMetric) {
	case "il":
		string += " - Internal Links: " + node.node.internalLinks;
		break;
	case "el":
		string += " - External Links: " + node.node.externalLinks;
		break;
	case "fl":
		string += " - Faulty Links: " + node.node.faultyLinks;
		break;
	case "bl":
		string += " - Files: " + node.node.binaryNodes;
		break;
	default:
		break;        
	}
	if(!(GraphUI.widthMetric === GraphUI.heightMetric)) {
		switch(GraphUI.heightMetric) {
		case "il":
			string += " - Internal Links: " + node.node.internalLinks;
			break;
		case "el":
			string += " - External Links: " + node.node.externalLinks;
			break;
		case "fl":
			string += " - Faulty Links: " + node.node.faultyLinks;
			break;
		case "bl":
			string += " - Files: " + node.node.binaryNodes;
			break;
		default:
			break;        
		}
	}
	if(!(GraphUI.widthMetric === GraphUI.colorMetric || GraphUI.heightMetric === GraphUI.colorMetric)) {
		switch(GraphUI.colorMetric) {
		case "il":
			string += " - Internal Links: " + node.node.internalLinks;
			break;
		case "el":
			string += " - External Links: " + node.node.externalLinks;
			break;
		case "fl":
			string += " - Faulty Links: " + node.node.faultyLinks;
			break;
		case "bl":
			string += " - Files: " + node.node.binaryNodes;
			break;
		default:
			break;        
		}
	}

	string += "</p>";
	$("#inner_status_bar").append(string);
};

GraphUI.setWidth = function(metric) {
	GraphUI.widthMetric = metric;
	var min = 10,	//MIN
	max = 30,		//MAX
	widthMin,
	widthMax;
	MyGraph.graph.forEachNode(function(node) {
		switch(metric) {
		case "il":
			node.width = (node.node.internalLinks || 0);
			$("button[name='width']").html("Internal Links<span class='caret'></span>");
			break;
		case "el":
			node.width = (node.node.externalLinks || 0);
			$("button[name='width']").html("External Links<span class='caret'></span>");
			break;
		case "fl":
			node.width = (node.node.faultyLinks || 0);
			$("button[name='width']").html("Faulty Links<span class='caret'></span>");
			break;
		case "bl":
			node.width = (node.node.binaryNodes || 0) ;
			$("button[name='width']").html("Files<span class='caret'></span>");
			break;
		default:
			node.width = 0;
		$("button[name='width']").html("Default<span class='caret'></span>");
		break;
		}
	});
	MyGraph.graph.forEachNode(function(node) {
		if(widthMin === undefined || node.width < widthMin)
			widthMin = node.width;
		if(widthMax === undefined || node.width > widthMax)
			widthMax = node.width;
	});

	MyGraph.graph.forEachNode(function(node) {
		if(widthMax == widthMin)
			var width = 10;
		else
			var width = ((max - min) * (node.width - widthMin) / (widthMax - widthMin)) + min;
		node.width = undefined;
		var ui = MyGraph.graphics.getNodeUI(node.id);
		var rect = ui.childNodes[0];
		ui.setAttribute("width", width);
		rect.setAttribute("width", width);
	});    
	MyGraph.renderer.rerender();
};

GraphUI.setHeight = function(metric) {
	GraphUI.heightMetric = metric;
	var min = 10,	//MIN
	max = 30,		//MAX
	heightMin,
	heightMax;
	MyGraph.graph.forEachNode(function(node) {
		switch(metric) {
		case "il":
			node.height = (node.node.internalLinks || 0);
			$("button[name='height']").html("Internal Links<span class='caret'></span>");
			break;
		case "el":
			node.height = (node.node.externalLinks || 0);
			$("button[name='height']").html("External Links<span class='caret'></span>");
			break;
		case "fl":
			node.height = (node.node.faultyLinks || 0);
			$("button[name='height']").html("Faulty Links<span class='caret'></span>");
			break;
		case "bl":
			node.height = (node.node.binaryNodes || 0);
			$("button[name='height']").html("Files<span class='caret'></span>");
			break;
		default:
			node.height = 0;
		$("button[name='height']").html("Default<span class='caret'></span>");
		break;
		}
	});
	MyGraph.graph.forEachNode(function(node) {
		if(heightMin === undefined || node.height < heightMin)
			heightMin = node.height;
		if(heightMax === undefined || node.height > heightMax)
			heightMax = node.height;
	});

	MyGraph.graph.forEachNode(function(node) {
		if(heightMax == heightMin)
			var height = 10;
		else
			var height = ((max - min) * (node.height - heightMin) / (heightMax - heightMin)) + min;
		node.height = undefined;
		var ui = MyGraph.graphics.getNodeUI(node.id);
		var rect = ui.childNodes[0];
		ui.setAttribute("height", height);
		rect.setAttribute("height", height);
	});  
	MyGraph.renderer.rerender();
};

GraphUI.setColor = function(metric) {
	var min = 0,	//MIN
	max = 255,		//MAX
	colorMin,
	colorMax;
	GraphUI.colorMetric = metric;
	MyGraph.graph.forEachNode(function(node) {
		var ui = MyGraph.graphics.getNodeUI(node.id);
		var rect = ui.childNodes[0];
		switch(metric) {
		case "il":
			node.color = node.node.internalLinks || 0;
			$("button[name='color']").html("Internal Links<span class='caret'></span>");
			break;
		case "el":
			node.color = node.node.externalLinks || 0;
			$("button[name='color']").html("External Links<span class='caret'></span>");
			break;
		case "fl":
			node.color = node.node.faultyLinks || 0;
			$("button[name='color']").html("Faulty Links<span class='caret'></span>");
			break;
		case "bl":
			node.color = node.node.binaryNodes || 0;
			$("button[name='color']").html("Files<span class='caret'></span>");
			break;
		default:
			node.color = 0;
		$("button[name='color']").html("Default<span class='caret'></span>");
		break;        
		};
	}); 

	MyGraph.graph.forEachNode(function(node) {
		if(colorMin === undefined || node.color < colorMin)
			colorMin = node.color;
		if(colorMax === undefined || node.color > colorMax)
			colorMax = node.color;
	});
	MyGraph.graph.forEachNode(function(node) {
		if(colorMax == colorMin)
			var color = 255;
		else
			var color = Math.floor(255- ((max - min) * (node.color - colorMin) / (colorMax - colorMin)) + min);
		node.color = undefined;
		var ui = MyGraph.graphics.getNodeUI(node.id);
		var rect = ui.childNodes[0];
		rect.setAttribute("fill", "rgb("+color+","+color+","+color+")");         
	}); 
	MyGraph.renderer.rerender();
};

GraphUI.showFaultyModal = function() {
	MyGraph.requestJSON("getFaultyLinksList?id="+$('#context_menu').data('node').id).then(function(message) {
		$('#graphModalBody').html(message.html);
	});
	$('#graphModal').modal('show');
	$('#graphModalLabel').html('Faulty Links');
	$('#graphModalSubLabel').html($('#context_menu').data('node').node.url);
	$('#context_menu').hide();
};

GraphUI.showFilesModal = function() {
	MyGraph.requestJSON("getFilesList?id="+$('#context_menu').data('node').id).then(function(message) {
		$('#graphModalBody').html(message.html);
	});
	$('#graphModal').modal('show');
	$('#graphModalLabel').html('Files');
	$('#graphModalSubLabel').html($('#context_menu').data('node').node.url);
	$('#context_menu').hide();
};

GraphUI.killNode = function() {
	var node = $("#context_menu").data("node");
	MyGraph.graph.removeNode(node.id);
};

GraphUI.killSelectedNodes = function() {
	MyGraph.selectedNodes.forEach(function(node) {
		MyGraph.graph.removeNode(node.id);
	});
	$("#group_context_menu").hide();
};
