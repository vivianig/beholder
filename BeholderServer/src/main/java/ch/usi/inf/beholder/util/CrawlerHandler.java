package ch.usi.inf.beholder.util;

import java.util.concurrent.Executors;

import ch.usi.inf.beholder.AbstractState;
import ch.usi.inf.beholder.Beholder;
import ch.usi.inf.beholder.utils.JobState;

public class CrawlerHandler {

	private static CrawlerHandler instance;

	private final Beholder crawler;

	public CrawlerHandler() {
		this.crawler = new Beholder();
		Executors.newCachedThreadPool().submit(this.crawler);
	}

	public String crawl(final String url, final String username) {
		final JobState result = this.crawler.addUrlToQueue(url, username);
		switch (result) {
			case ACCEPTED:
				return "{\"status\" : 0}";
			case ERROR:
				return "{\"status\" : 1, \"html\" : \"<div class='crawler_message alert alert-danger fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>Error:</strong> An error has occured while trying to add the following job: "
						+ url + "</div>\"}";
			case DUPLICATE:
				return "{\"status\" : 2, \"html\" : \"<div class='crawler_message alert alert-danger fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>Error:</strong> The job "
						+ url + " is already present in the queue</div>\"}";
			default:
				return "{\"status\" : 0}";
		}
	}

	public AbstractState getState(final String url, final String user) {
		return this.crawler.getState(url, user);
	}

	public AbstractState getCurrentState(final String user) {
		return this.crawler.getCurrentState(user);
	}

	public boolean cancelJob(final String url) {
		return this.crawler.cancelJob(url);
	}

	public static CrawlerHandler getInstance() {
		if (instance == null)
			instance = new CrawlerHandler();
		return instance;
	}

}
