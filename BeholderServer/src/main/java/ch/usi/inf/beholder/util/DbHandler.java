package ch.usi.inf.beholder.util;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.util.JSON;

public class DbHandler {

	private static DbHandler instance = null;

	private DBCollection htmlNodes;
	private DBCollection binaryNodes;
	private DBCollection edges;
	private DBCollection graphs;
	private DBCollection faultyLinks;
	private DBCollection users;

	private HashMap<String, Graph> loadedGraphs;

	public DbHandler() {
		try {
			final MongoCredential credential = MongoCredential
					.createMongoCRCredential("beholder", "beholder",
							"b3h0ld3r".toCharArray());
			final MongoClient mongoClient = new MongoClient(new ServerAddress(
					"127.0.0.1"), Arrays.asList(credential));
			final DB db = mongoClient.getDB("beholder");
			this.htmlNodes = db.getCollection("htmlNodes");
			this.binaryNodes = db.getCollection("binaryNodes");
			this.edges = db.getCollection("edges");
			this.graphs = db.getCollection("graphs");
			this.faultyLinks = db.getCollection("faultyLinks");
			this.users = db.getCollection("users");
			this.loadedGraphs = new HashMap<String, Graph>();
		} catch (final UnknownHostException e) {
			e.printStackTrace();
		}
	}

	public DBObject getUser(final String username, final String password) {
		final DBObject entry = new BasicDBObject("username", username).append(
				"password", password);
		return this.users.findOne(entry);
	}

	public boolean userExist(final String username) {
		final DBObject entry = new BasicDBObject("username", username);
		return (this.users.findOne(entry) != null);
	}

	public void addUser(final String username, final String password) {
		final DBObject user = new BasicDBObject("username", username).append(
				"password", password);
		this.users.insert(user);
	}

	public String getBinaryNodes(final String id) {
		final DBObject query = new BasicDBObject("parent", id);
		final DBCursor curs = this.binaryNodes.find(query);
		final BasicDBList results = new BasicDBList();
		while (curs.hasNext()) {
			results.add(curs.next());
		}
		return JSON.serialize(results);
	}

	public DBObject getOrigin(final String graph, final String graphUrl) {
		final DBObject query = new BasicDBObject("graph", graph).append("url",
				graphUrl);
		return this.htmlNodes.findOne(query);
	}

	public HashMap<String, DBObject> getNodes(final String graph) {
		final DBObject query = new BasicDBObject("graph", graph);
		final DBCursor curs = this.htmlNodes.find(query);
		final HashMap<String, DBObject> results = new HashMap<>();
		while (curs.hasNext()) {
			final DBObject next = curs.next();
			results.put((String) next.get("id"), next);
		}
		return results;
	}

	public HashMap<String, BasicDBList> getEdges(final String graph) {
		final DBObject query = new BasicDBObject("graph", graph);
		final DBCursor curs = this.edges.find(query);
		final HashMap<String, BasicDBList> results = new HashMap<>();
		while (curs.hasNext()) {
			final DBObject next = curs.next();
			final String from = (String) next.get("source");
			if (results.get(from) == null) {
				final BasicDBList list = new BasicDBList();
				list.add(next);
				results.put(from, list);
			} else {
				results.get(from).add(next);
			}

		}
		return results;
	}

	public List<GraphInformation> getGraphs(final String user) {
		final DBObject query = new BasicDBObject("user", user);
		final DBCursor curs = this.graphs.find(query);
		final List<GraphInformation> results = new ArrayList<>();
		while (curs.hasNext()) {
			final DBObject next = curs.next();
			final GraphInformation graph = new GraphInformation(next);
			results.add(graph);
		}
		return results;
	}

	public Graph getGraph(final String graphId, final String user) {
		final DBObject query = new BasicDBObject("id", graphId).append("user",
				user);
		final DBObject graph = this.graphs.findOne(query);
		if (graph == null)
			return null;
		final Graph loadedGraph = this.loadedGraphs.get(graphId);
		if (loadedGraph != null
				&& loadedGraph.getDate().equals(graph.get("date"))) {
			if (!loadedGraph.getUsers().contains(user))
				loadedGraph.getUsers().add(user);
			System.out.println(loadedGraph.getDate() + " "
					+ loadedGraph.getUsers() + " " + loadedGraph.getGraphUrl());
			return loadedGraph;
		}
		final Graph newGraph = new Graph((String) graph.get("id"),
				(String) graph.get("url"), (List<String>) graph.get("user"),
				(String) graph.get("date"));
		System.out.println(newGraph.getDate() + " " + newGraph.getUsers() + " "
				+ newGraph.getGraphUrl());
		this.loadedGraphs.put(graphId, newGraph);
		return newGraph;
	}

	public ArrayList<DBObject> getFaultyLinks(final String id) {
		final DBObject query = new BasicDBObject("parent", id);
		final DBCursor curs = this.faultyLinks.find(query);
		final ArrayList<DBObject> results = new ArrayList<>();
		while (curs.hasNext()) {
			final DBObject next = curs.next();
			results.add(next);
		}
		return results;
	}

	public ArrayList<DBObject> getFiles(final String id) {
		final DBObject query = new BasicDBObject("parent", id);
		final DBCursor curs = this.binaryNodes.find(query);
		final ArrayList<DBObject> results = new ArrayList<>();
		while (curs.hasNext()) {
			final DBObject next = curs.next();
			results.add(next);
		}
		return results;
	}

	public void removeGraph(final String graphId, final String user) {
		final BasicDBObject graphQuery = new BasicDBObject("id", graphId)
				.append("user", user);
		final DBObject graph = this.graphs.findOne(graphQuery);
		if (graph != null) {
			final BasicDBObject query = new BasicDBObject("graph",
					graph.get("id"));
			if ((((BasicDBList) graph.get("user")).size()) > 1) {
				final BasicDBObject update = new BasicDBObject("user", user);
				this.graphs.update(query, new BasicDBObject("$pull", update));
				this.loadedGraphs.get(graphId).getUsers().remove(user);
			} else {
				this.binaryNodes.remove(query);
				this.htmlNodes.remove(query);
				this.faultyLinks.remove(query);
				this.edges.remove(query);
				this.graphs.remove(graph);
				this.loadedGraphs.remove(graphId);
			}
		}
	}

	public void purge(final boolean users) {
		this.binaryNodes.drop();
		this.edges.drop();
		this.faultyLinks.drop();
		this.graphs.drop();
		this.htmlNodes.drop();
		this.loadedGraphs.clear();
		if (users)
			this.users.drop();
	}

	public static DbHandler getInstance() {
		if (instance == null)
			instance = new DbHandler();
		return instance;
	}
}
