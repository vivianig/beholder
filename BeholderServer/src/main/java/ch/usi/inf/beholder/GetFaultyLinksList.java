package ch.usi.inf.beholder;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

import com.mongodb.DBObject;

@Path("/getFaultyLinksList")
public class GetFaultyLinksList {

	@GET
	@Produces("application/json")
	public Response get(@QueryParam("id") final String id) {
		final ArrayList<DBObject> faultyLinks = DbHandler.getInstance()
				.getFaultyLinks(id);
		String message = "{\"html\" : \"<div class='panel-group' id='accordion'>";
		for (int i = 0; i < faultyLinks.size(); ++i) {
			message += this.generateHtmlUrl(faultyLinks.get(i), i);
		}
		message += "</div>\"}";
		return Response.ok().entity(message).build();
	}

	private String generateHtmlUrl(final DBObject link, final int index) {
		return "<div class='panel panel-default'>"
				+ "<div class='panel-heading'>"
				+ "<h4 class='panel-title'>"
				+ "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"
				+ index
				+ "'>"
				+ ((String) link.get("url"))
				+ "</a></h4></div>"
				+ "<div id='collapse"
				+ index
				+ "' class='panel-collapse collapse'>"
				+ "<div class='panel-body'>"
				+ "<h5>Faulty Tag</h5>"
				+ ((String) link.get("code")).replaceAll("\"", "\\\\\"")
						.replaceAll("<", "&lt").replaceAll(">", "&gt")
				+ "</div></div></div>";
	}
}
