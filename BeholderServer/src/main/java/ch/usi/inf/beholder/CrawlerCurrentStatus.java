package ch.usi.inf.beholder;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.CrawlerHandler;

@Path("/crawlerCurrentStatus")
public class CrawlerCurrentStatus {

	@GET
	@Produces("application/json")
	public Response get(@CookieParam("beholder") final String username) {
		final AbstractState state = CrawlerHandler.getInstance()
				.getCurrentState(username);
		if (state == null)
			return Response.ok().entity("{\"status\" : 0}").build();
		return Response.ok()
				.entity("{\"status\" : 1, \"url\":\"" + state.getUrl() + "\"}")
				.build();

	}
}
