package ch.usi.inf.beholder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

import com.sun.jersey.api.view.Viewable;

@Path("/purge")
public class Purge {

	@GET
	public Response get(@QueryParam("password") final String password,
			@QueryParam("users") final String users) {
		// b3h0ld3rR3V34L
		if (password != null && password.hashCode() == 821328442) {
			if (users != null && users.equals("true")) {
				DbHandler.getInstance().purge(true);
			} else {
				DbHandler.getInstance().purge(false);
			}
			System.out.println("Db purged");
		}
		return Response.ok(new Viewable("/index.jsp", null)).build();
	}

}
