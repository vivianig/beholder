package ch.usi.inf.beholder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

@Path("/binaryNodes")
public class BinaryNodes {

	@GET
	@Produces("application/json")
	public Response get(@QueryParam("id") final String id) {
		final String json = DbHandler.getInstance().getBinaryNodes(id);
		return Response.ok().entity(json).build();
	}
}
