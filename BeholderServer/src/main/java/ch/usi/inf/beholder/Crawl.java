package ch.usi.inf.beholder;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.CrawlerHandler;

@Path("/crawl")
public class Crawl {

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response post(final MultivaluedMap<String, String> form,
			@CookieParam("beholder") final String username) {
		return Response
				.ok()
				.entity(CrawlerHandler.getInstance().crawl(
						form.getFirst("url"), username)).build();
	}
}
