package ch.usi.inf.beholder.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Graph {

	private final HashMap<String, DBObject> nodes;
	private final HashMap<String, BasicDBList> edges;
	private final DBObject origin;
	private final DBObject graph;
	private final List<String> users;
	private final String url;
	private final String date;
	private final String graphId;
	private final String graphUrl;

	public Graph(final String graphId, final String graphUrl,
			final List<String> users, final String date) {
		System.out.println("Initializing graph " + graphId);
		this.users = users;
		this.date = date;
		this.url = graphUrl;
		this.graphId = graphId;
		this.graphUrl = graphUrl;

		final DbHandler dbHandler = DbHandler.getInstance();
		this.nodes = dbHandler.getNodes(graphId);
		this.edges = dbHandler.getEdges(graphId);
		this.origin = dbHandler.getOrigin(graphId, graphUrl);
		this.graph = new BasicDBObject();
		this.radialLayout();
		System.out.println("Graph Initialized");
	}

	public String toJSON() {
		return JSON.serialize(this.graph);
	}

	private void radialLayout() {
		System.out.println("Creating layout");
		final HashSet<String> visited = new HashSet<>();
		final LinkedList<DBObject> queue = new LinkedList<>();
		queue.add(this.origin);
		visited.add((String) this.origin.get("id"));
		this.origin.put("alpha", 0.0);
		this.origin.put("beta", 2 * Math.PI);
		this.origin.put("level", 1);
		this.origin.put("x", 0.0);
		this.origin.put("y", 0.0);
		this.origin.put("distance", 0.0);

		final BasicDBList nodes = new BasicDBList();
		nodes.add(this.origin);

		final BasicDBList edges = new BasicDBList();
		for (final BasicDBList edgeList : this.edges.values()) {
			for (final Object edge : edgeList) {
				if (!edges.contains(edge)) {
					edges.add(edge);
				}
			}
		}

		while (!queue.isEmpty()) {
			final DBObject nextNode = queue.pop();
			this.computePosition(nextNode, visited, queue, nodes);
		}

		this.graph.put("nodes", nodes);
		this.graph.put("edges", edges);
		this.graph.put("url", this.url);
	}

	private void computePosition(final DBObject node,
			final HashSet<String> visited, final LinkedList<DBObject> queue,
			final BasicDBList nodes) {
		final BasicDBList edges = this.edges.get(node.get("id"));
		final BasicDBList children = new BasicDBList();
		if (edges != null) {
			for (final Object edge : edges) {
				final String childId = (String) ((DBObject) edge).get("target");
				if (this.nodes.get(childId) != null) {
					final DBObject child = this.nodes.get(childId);
					if (!visited.contains(childId)) {
						visited.add(childId);
						if (!queue.contains(child)) {// TODO really necessary?
							queue.add(child);
							children.add(child);
							nodes.add(child);
						}
					}
				}
			}
		}
		final int n = children.size();
		if (n < 1) {
			return;
		}
		final int level = (int) node.get("level");
		final double beta = ((double) node.get("beta"));
		final double alpha = ((double) node.get("alpha"));
		double distance = ((double) node.get("distance"));
		double gamma;
		if (level > 1)
			gamma = (beta - alpha) / (n + 1);
		else
			gamma = (beta - alpha) / n;
		distance += Math.max(Math.min(500, (50 * n / (beta - alpha))), 100);
		for (int i = 1; i < n + 1; ++i) {
			final DBObject child = (DBObject) children.get(i - 1);
			final double angle = alpha + gamma * i;
			final double x = distance * Math.cos(angle);
			final double y = distance * Math.sin(angle);

			child.put("x", x);
			child.put("y", y);
			child.put("level", level + 1);

			final double childAlpha = angle - gamma / 2;
			final double childBeta = angle + gamma / 2;
			child.put("alpha", childAlpha);
			child.put("beta", childBeta);

			child.put("distance", distance);
		}
	}

	public List<String> getUsers() {
		return this.users;
	}

	public String getDate() {
		return this.date;
	}

	public String getGraphId() {
		return this.graphId;
	}

	public String getGraphUrl() {
		return this.graphUrl;
	}
}
