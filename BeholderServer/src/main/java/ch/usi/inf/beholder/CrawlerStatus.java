package ch.usi.inf.beholder;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.CrawlerHandler;

@Path("/crawlerStatus")
public class CrawlerStatus {

	@GET
	@Produces("application/json")
	public Response get(@QueryParam("url") final String url,
			@CookieParam("beholder") final String username) {
		final AbstractState state = CrawlerHandler.getInstance().getState(url,
				username);
		if (state == null)
			return Response.ok().build();
		return Response.ok().entity(state.getState()).build();

	}
}
