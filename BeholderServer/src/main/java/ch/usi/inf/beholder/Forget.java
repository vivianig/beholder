package ch.usi.inf.beholder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.view.Viewable;

@Path("/forget")
public class Forget {

	@GET
	@Produces("text/html")
	public Response get() {
		return Response
				.ok(new Viewable("/index.jsp", null))
				.header("Set-Cookie",
						"beholder=deleted;Expires=Thu, 01-Jan-1970 00:00:01 GMT")
				.build();
	}

}
