package ch.usi.inf.beholder;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

import com.mongodb.DBObject;

@Path("/getFilesList")
public class GetFilesList {

	@GET
	@Produces("application/json")
	public Response get(@QueryParam("id") final String id) {
		final ArrayList<DBObject> files = DbHandler.getInstance().getFiles(id);
		String message = "{\"html\" : \"<div class='panel-group' id='accordion'>";
		for (int i = 0; i < files.size(); ++i) {
			message += this.generateHtmlUrl(files.get(i), i);
		}
		message += "</div>\"}";
		return Response.ok().entity(message).build();
	}

	private String generateHtmlUrl(final DBObject file, final int index) {
		return "<div class='panel panel-default'>"
				+ "<div class='panel-heading'>"
				+ "<h4 class='panel-title'>"
				+ "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"
				+ index + "'>" + ((String) file.get("label"))
				+ "</a></h4></div>" + "<div id='collapse" + index
				+ "' class='panel-collapse collapse'>"
				+ "<div class='panel-body'>" + "<h5>File Size</h5>"
				+ this.convertToHumanReadable((long) file.get("size"))
				+ "</div></div></div>";
	}

	private String convertToHumanReadable(final long bytes) {
		if (bytes < 1024)
			return "" + bytes;
		final int exp = (int) (Math.log(bytes) / Math.log(1024));
		final char prefix = ("KMGTPE").charAt(exp - 1);
		return (int) (bytes / Math.pow(1024, exp)) + " " + prefix + "B";
	}
}
