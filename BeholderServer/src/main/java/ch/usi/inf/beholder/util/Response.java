package ch.usi.inf.beholder.util;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Response {

	private final String text;
	private final long time;

	public Response(final String text) {
		this.text = text;
		this.time = new Date().getTime();
	}

	public String getText() {
		return this.text;
	}

	public long getTime() {
		return this.time;
	}

}
