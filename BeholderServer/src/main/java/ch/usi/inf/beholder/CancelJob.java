package ch.usi.inf.beholder;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.CrawlerHandler;

@Path("/cancelJob")
public class CancelJob {
	@POST
	@Produces("application/json")
	public Response post(@QueryParam("url") final String url) {
		final boolean result = CrawlerHandler.getInstance().cancelJob(url);
		if (result) {
			return Response
					.ok()
					.entity("{\"status\" : 0, \"html\" : \"<div class='crawler_message alert alert-danger fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>Job Stopped</strong> URL: "
							+ url + "</div>\"}").build();
		} else {
			return Response
					.ok()
					.entity("{\"status\" : 1, \"html\" : \"<div class='crawler_message alert alert-danger fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>Error:</strong> The crawler was unable to stop the job with the URL: "
							+ url
							+ ". The job might be already been finished</div>\"}")
					.build();
		}
	}

}
