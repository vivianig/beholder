package ch.usi.inf.beholder;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;
import ch.usi.inf.beholder.util.GraphInformation;

import com.sun.jersey.api.view.Viewable;

@Path("/")
public class Index {

	@GET
	@Produces("text/html")
	public Response get(@CookieParam("beholder") final String username,
			@Context final HttpServletRequest request) {
		if (DbHandler.getInstance().userExist(username)) {
			final List<GraphInformation> graphs = DbHandler.getInstance().getGraphs(
					username);
			request.setAttribute("graphs", graphs);
			return Response.ok(new Viewable("/user.jsp", null)).build();
		} else {
			return Response
					.ok(new Viewable("/index.jsp", null))
					.header("Set-Cookie",
							"beholder=deleted;Expires=Thu, 01-Jan-1970 00:00:01 GMT")
					.build();
		}
	}
}
