package ch.usi.inf.beholder;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

@Path("/register")
public class Register {
	@POST
	@Produces("text/html")
	@Consumes("application/x-www-form-urlencoded")
	public Response post(@Context final HttpServletRequest request,
			final MultivaluedMap<String, String> form) {
		final HttpSession session = request.getSession(true);
		if (!DbHandler.getInstance().userExist(form.getFirst("username"))) {
			DbHandler.getInstance().addUser(form.getFirst("username"),
					form.getFirst("password"));
			session.setAttribute("error", "Invalid User Or Password");
			return Response
					.seeOther(URI.create("/"))
					.cookie(new NewCookie("beholder", form.getFirst("username")))
					.build();
		} else {
			session.setAttribute("error", "User already exist");
			return Response.seeOther(URI.create("/")).build();
		}
	}
}
