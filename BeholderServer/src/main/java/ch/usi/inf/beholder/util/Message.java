package ch.usi.inf.beholder.util;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Message {

	private final String message;

	public Message(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

}
