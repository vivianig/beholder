package ch.usi.inf.beholder;

import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

@Path("/removeGraph")
public class RemoveGraph {
	@POST
	@Produces("text/html")
	public Response post(@QueryParam("id") final String id,
			@CookieParam("beholder") final String username) {
		DbHandler.getInstance().removeGraph(id, username);
		return Response.ok().build();
	}
}
