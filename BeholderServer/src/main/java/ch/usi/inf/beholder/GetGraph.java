package ch.usi.inf.beholder;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;
import ch.usi.inf.beholder.util.Graph;

@Path("/getGraph")
public class GetGraph {

	@GET
	@Produces("application/json")
	public Response get(@QueryParam("id") final String id,
			@CookieParam("beholder") final String username) {
		final Graph graph = DbHandler.getInstance().getGraph(id, username);
		System.out.println(id + " " + username + " " + graph);
		final String json = graph.toJSON();
		return Response.ok().entity(json).build();
	}
}
