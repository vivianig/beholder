package ch.usi.inf.beholder;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.view.Viewable;

@Path("/graph")
public class Graph {

	@GET
	@Produces("text/html")
	public Response get(@QueryParam("id") final String id) {
		return Response.ok(new Viewable("/graph.jsp")).build();
	}
}
