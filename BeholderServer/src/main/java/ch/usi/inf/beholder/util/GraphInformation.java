package ch.usi.inf.beholder.util;

import java.util.List;

import com.mongodb.DBObject;

public class GraphInformation {

	final private String url;
	final private List<String> users;
	final private int links;
	final private int externalLinks;
	final private int internalLinks;
	final private int faultyLinks;
	final private int files;
	final private String date;
	final private String id;

	public GraphInformation(final DBObject graphEntry) {
		this.url = (String) graphEntry.get("url");
		this.users = (List<String>) graphEntry.get("users");
		this.links = (int) graphEntry.get("links");
		this.externalLinks = (int) graphEntry.get("externalLinks");
		this.internalLinks = (int) graphEntry.get("internalLinks");
		this.faultyLinks = (int) graphEntry.get("faultyLinks");
		this.files = (int) graphEntry.get("files");
		this.date = (String) graphEntry.get("date");
		this.id = (String) graphEntry.get("id");
	}

	public String getUrl() {
		return this.url;
	}

	public List<String> getUsers() {
		return this.users;
	}

	public int getLinks() {
		return this.links;
	}

	public int getExternalLinks() {
		return this.externalLinks;
	}

	public int getInternalLinks() {
		return this.internalLinks;
	}

	public int getFaultyLinks() {
		return this.faultyLinks;
	}

	public int getFiles() {
		return this.files;
	}

	public String getDate() {
		return this.date;
	}

	public String getId() {
		return this.id;
	}
}
