package ch.usi.inf.beholder;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;

import ch.usi.inf.beholder.util.DbHandler;

import com.mongodb.DBObject;

@Path("/login")
public class Login {

	@POST
	@Produces("text/html")
	@Consumes("application/x-www-form-urlencoded")
	public Response post(@Context final HttpServletRequest request,
			final MultivaluedMap<String, String> form) {
		final DBObject user = DbHandler.getInstance().getUser(
				form.getFirst("username"), form.getFirst("password"));
		if (user == null) {
			final HttpSession session = request.getSession(true);
			session.setAttribute("error", "Invalid Login");
			return Response.seeOther(URI.create("/")).build();
		} else {
			// TODO better cookie pls
			return Response
					.seeOther(URI.create("/"))
					.cookie(new NewCookie("beholder", form.getFirst("username")))
					.build();
		}
	}
}