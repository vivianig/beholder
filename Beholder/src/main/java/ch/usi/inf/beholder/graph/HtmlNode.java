package ch.usi.inf.beholder.graph;

import java.util.concurrent.atomic.AtomicInteger;

import org.jsoup.nodes.Document;

public class HtmlNode extends AbstractNode {

	private final AtomicInteger internalEdgesCount;
	private final AtomicInteger externalEdgesCount;
	private final AtomicInteger faultyLinksCount;
	private final AtomicInteger binaryNodesCount;
	private Document doc;
	private String label;

	public HtmlNode(final String url) {
		super(url);
		this.internalEdgesCount = new AtomicInteger();
		this.externalEdgesCount = new AtomicInteger();
		this.faultyLinksCount = new AtomicInteger();
		this.binaryNodesCount = new AtomicInteger();
		this.label = url.substring(this.url.lastIndexOf('/') + 1);
	}

	public void increaseInternalEdgesCount() {
		this.internalEdgesCount.incrementAndGet();
	}

	public void increaseExternalEdgesCount() {
		this.externalEdgesCount.incrementAndGet();
	}

	public void increaseFaultyLinksCount() {
		this.faultyLinksCount.incrementAndGet();
	}

	public void increaseBinaryNodesCount() {
		this.binaryNodesCount.incrementAndGet();
	}

	public int getInternalEdgesCount() {
		return this.internalEdgesCount.get();
	}

	public int getExternalEdgesCount() {
		return this.externalEdgesCount.get();
	}

	public int getFaultyLinksCount() {
		return this.faultyLinksCount.get();
	}

	public int getBinaryNodesCount() {
		return this.binaryNodesCount.get();
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	@Override
	public String getUrl() {
		return this.url;
	}

	@Override
	public int getColor() {
		return 100;
	}

	@Override
	public String getContentType() {
		return "text/html";
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof HtmlNode))
			return false;
		return this.url.equals(((HtmlNode) obj).getUrl());
	}

	public Document getDoc() {
		return this.doc;
	}

	public void setDoc(final Document doc) {
		this.doc = doc;
	}

	public void setLabel(final String label) {
		this.label = label;
	}

}
