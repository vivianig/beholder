package ch.usi.inf.beholder.graph;

public class ExternalNode extends AbstractNode {

	public ExternalNode(final String url) {
		super(url);
	}

	@Override
	public String getLabel() {
		return this.url.substring(this.url.lastIndexOf('/') + 1);
	}

	@Override
	public String getUrl() {
		return this.url;
	}

	@Override
	public int getColor() {
		return 0;
	}

	// Not called
	@Override
	public String getContentType() {
		return null;
	}

}
