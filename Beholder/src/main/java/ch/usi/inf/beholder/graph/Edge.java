package ch.usi.inf.beholder.graph;

public class Edge {

	private final AbstractNode startingNode;
	private final AbstractNode endingNode;
	private int strength;

	public Edge(final AbstractNode startingNode, final AbstractNode endingNode) {
		this.startingNode = startingNode;
		this.endingNode = endingNode;
		this.strength = 1;
	}

	public AbstractNode getStartingNode() {
		return this.startingNode;
	}

	public AbstractNode getEndingNode() {
		return this.endingNode;
	}

	public void increaseStrength() {
		this.strength++;
	}

	public int getStrength() {
		return this.strength;
	}

	@Override
	public boolean equals(final Object obj) {
		if (obj == null)
			return false;
		if (obj == this)
			return true;
		if (!(obj instanceof Edge))
			return false;
		return this.startingNode == ((Edge) obj).getStartingNode()
				&& this.endingNode == ((Edge) obj).getEndingNode();
	}

	@Override
	public int hashCode() {
		return (this.startingNode.getUrl() + this.endingNode.getUrl())
				.hashCode();
	}
}
