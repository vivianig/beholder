package ch.usi.inf.beholder;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.commons.validator.routines.UrlValidator;

import ch.usi.inf.beholder.utils.JobState;

public class Beholder implements Runnable {

	private final Crawler crawler;
	private final LinkedBlockingQueue<CrawlingJob> queue;
	private final HashMap<String, AbstractState> crawlerStates;
	private String currentUrl;
	private String currentUser;

	public Beholder() {
		System.setProperty("jsse.enableSNIExtension", "false");
		this.queue = new LinkedBlockingQueue<>();
		this.crawlerStates = new HashMap<>();
		this.crawler = new Crawler();
	}

	@Override
	public void run() {
		while (true) {
			try {
				this.crawl(this.queue.take());
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public JobState addUrlToQueue(final String url, final String user) {
		if (this.queue.contains(new CrawlingJob(user, url))
				|| url.equals(this.currentUrl))
			return JobState.DUPLICATE;
		if (!UrlValidator.getInstance().isValid(url))
			return JobState.ERROR;
		final AbstractState state = new RunningState(url);
		try {
			this.crawlerStates.put(url, state);
			this.queue.put(new CrawlingJob(user, url));
			System.out.println(url + " " + state);
		} catch (final InterruptedException e) {
			e.printStackTrace();
			return JobState.ERROR;
		}
		return JobState.ACCEPTED;
	}

	private void crawl(final CrawlingJob crawlingJob) {
		this.currentUrl = crawlingJob.getUrl();
		this.currentUser = crawlingJob.getUser();
		;
		try {
			final JobState status = this.crawler.crawl(this.currentUrl,
					this.currentUser, this.crawlerStates.get(this.currentUrl));
			if (status.equals(JobState.ACCEPTED)) {
				System.out.println("Graph stored");
				this.crawlerStates.put(this.currentUrl, new CompletedState(
						this.crawler.storeGraph()));
			} else {
				this.crawlerStates.put(this.currentUrl, new ErrorState(
						this.currentUrl));
			}
		} catch (IllegalArgumentException | IOException e) {
			this.crawlerStates.put(this.currentUrl, new ErrorState(
					this.currentUrl));
			e.printStackTrace();
		}
		System.out.println("job done");
		this.currentUrl = null;
		this.currentUser = null;

	}

	public AbstractState getState(final String url, final String user) {
		return this.crawlerStates.get(url);
	}

	public AbstractState getCurrentState(final String url) {
		return this.crawlerStates.get(this.currentUrl);
	}

	public boolean cancelJob(final String url) {
		if (url.equals(this.currentUrl) || (url + "/").equals(this.currentUrl)) {
			this.crawler.stopCrawling();
			return true;
		} else {
			return false;
		}

	}

	protected class CrawlingJob {
		private final String user;
		private final String url;

		public CrawlingJob(final String user, final String url) {
			this.user = user;
			this.url = url;
		}

		public String getUser() {
			return this.user;
		}

		public String getUrl() {
			return this.url;
		}

		@Override
		public boolean equals(final Object obj) {
			if (!(obj instanceof CrawlingJob))
				return false;
			return this.url.equals(((CrawlingJob) obj).getUrl())
					&& this.user.equals(((CrawlingJob) obj).getUser());
		}
	}
}
