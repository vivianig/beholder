package ch.usi.inf.beholder.graph;

public class FaultyLink {

	private final String url;
	private final AbstractNode parent;
	private final String code;

	public FaultyLink(final String url, final AbstractNode parent,
			final String code) {
		this.url = url;
		this.parent = parent;
		this.code = code;
	}

	public String getUrl() {
		return this.url;
	}

	public AbstractNode getParent() {
		return this.parent;
	}

	public String getCode() {
		return this.code;
	}

}
