package ch.usi.inf.beholder;

public abstract class AbstractState {

	public abstract States getType();

	public abstract String getState();

	public abstract String getUrl();

}
