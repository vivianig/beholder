package ch.usi.inf.beholder;

import java.util.ArrayList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class CompletedState extends AbstractState {

	private final DBObject graph;
	private final String url;

	public CompletedState(final BasicDBObject graph) {
		this.graph = graph;
		this.url = (String) graph.get("url");
	}

	@Override
	public States getType() {
		return States.DONE;
	}

	@Override
	public String getState() {
		return "{\"status\" : 1,"
				+ "\"user\" : \""
				+ ((ArrayList<String>) this.graph.get("user")).get(0)
				+ "\", "
				+ "\"html\" : \"<div class='graph_box'name='"
				+ this.graph.get("id")
				+ "'><table><button type='button' class='close'>&times;</button>"
				+ "<tbody><tr><td><label>Url</label> " + this.graph.get("url")
				+ "</td><td><label>Date</label> " + this.graph.get("date")
				+ "</td></tr><tr><td><label>Internal Links</label> "
				+ this.graph.get("internalLinks")
				+ "</td><td><label>External Links</label> "
				+ this.graph.get("externalLinks")
				+ "</td></tr><tr><td><label>Files</label> "
				+ this.graph.get("files")
				+ "</td><td><label>Faulty Links</label> "
				+ this.graph.get("faultyLinks")
				+ "</td></tr></tbody></table></div>\"}";
	}

	@Override
	public String getUrl() {
		return this.url;
	}
}
