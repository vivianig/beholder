package ch.usi.inf.beholder;

import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicInteger;

import ch.usi.inf.beholder.graph.AbstractBinaryNode;
import ch.usi.inf.beholder.graph.AbstractNode;
import ch.usi.inf.beholder.graph.Edge;
import ch.usi.inf.beholder.graph.FaultyLink;
import ch.usi.inf.beholder.graph.HtmlNode;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

public class DbHandler {

	private static DbHandler instance = null;

	private DBCollection htmlNodes;
	private DBCollection binaryNodes;
	private DBCollection edges;
	private DBCollection graphs;
	private DBCollection faultyLinks;
	private DBCollection counters;

	public DbHandler() {
		try {
			final MongoCredential credential = MongoCredential
					.createMongoCRCredential("beholder", "beholder",
							"b3h0ld3r".toCharArray());
			final MongoClient mongoClient = new MongoClient(new ServerAddress(
					"127.0.0.1"), Arrays.asList(credential));
			final DB db = mongoClient.getDB("beholder");

			this.graphs = db.getCollection("graphs");
			this.htmlNodes = db.getCollection("htmlNodes");
			this.binaryNodes = db.getCollection("binaryNodes");
			this.edges = db.getCollection("edges");
			this.faultyLinks = db.getCollection("faultyLinks");
			this.counters = db.getCollection("counters");

			this.checkCounters();

			System.out.println("Db initialized");
		} catch (final UnknownHostException e) {
			e.printStackTrace();
		}
	}

	private void checkCounters() {
		BasicDBObject query = new BasicDBObject("collection", "graph");
		DBObject counter = this.counters.findOne(query);
		if (counter == null) {
			query.append("counter", 0);
			this.counters.insert(query);
		}
		query = new BasicDBObject("collection", "htmlNodes");
		counter = this.counters.findOne(query);
		if (counter == null) {
			query.append("counter", 0);
			this.counters.insert(query);
		}
		query = new BasicDBObject("collection", "binaryNodes");
		counter = this.counters.findOne(query);
		if (counter == null) {
			query.append("counter", 0);
			this.counters.insert(query);
		}
	}

	private void updateGraphCounter() {
		final BasicDBObject query = new BasicDBObject("collection", "graph");
		final BasicDBObject update = new BasicDBObject("$inc",
				new BasicDBObject("counter", 1));
		this.counters.findAndModify(query, update);
	}

	public BasicDBObject addGraph(final String user, final String url,
			final ArrayList<HtmlNode> nodes,
			final ArrayList<AbstractBinaryNode> binaryNodes,
			final ArrayList<Edge> edges,
			final ArrayList<AbstractNode> externalNodes,
			final ArrayList<Edge> externalEdges,
			final ArrayList<FaultyLink> faultyLinks) {

		final BasicDBObject graph = new BasicDBObject("url", url);
		final ArrayList<String> users = new ArrayList<>();
		users.add(user);

		final AtomicInteger counter = new AtomicInteger((int) this.counters
				.findOne(new BasicDBObject("collection", "graph")).get(
						"counter"));
		final String graphId = "G-" + counter.getAndIncrement();
		graph.append("id", graphId);
		this.updateGraphCounter();

		System.out.println("graph entry created");

		final AtomicInteger htmlCounter = new AtomicInteger();
		final AtomicInteger binaryCounter = new AtomicInteger();
		final AtomicInteger edgesCounter = new AtomicInteger();

		for (int i = 0; i < nodes.size(); ++i) {
			final HtmlNode node = nodes.get(i);
			this.addNode(node, graphId, htmlCounter);
		}

		System.out.println("html nodes added");

		for (int i = 0; i < binaryNodes.size(); ++i) {
			final AbstractBinaryNode node = binaryNodes.get(i);
			this.addBinaryNode(node, graphId, binaryCounter);
		}

		System.out.println("binary nodes added");

		for (int i = 0; i < edges.size(); ++i) {
			this.addEdge(edges.get(i), graphId, edgesCounter);
		}

		System.out.println("edges added");

		for (final FaultyLink faultyLink : faultyLinks) {
			this.addFaultyLink(faultyLink, graphId);
		}

		System.out.println("faulty links added");

		graph.append("user", users)
				.append("links",
						nodes.size() + nodes.size() + binaryNodes.size()
								+ externalNodes.size())
				.append("externalLinks", externalNodes.size())
				.append("internalLinks", nodes.size())
				.append("faultyLinks", faultyLinks.size())
				.append("files", binaryNodes.size())
				.append("date",
						(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss"))
								.format(Calendar.getInstance().getTime()));
		this.graphs.insert(graph);
		return graph;

	}

	private BasicDBObject addFaultyLink(final FaultyLink faultyLink,
			final String graphId) {
		if (faultyLink == null || graphId == null)
			return null;
		final DBObject parentEntry = this.getNodeEntry(faultyLink.getParent(),
				graphId);
		if (parentEntry == null)
			return null;
		final BasicDBObject entry = new BasicDBObject("url",
				faultyLink.getUrl()).append("code", faultyLink.getCode())
				.append("graph", graphId);
		final ArrayList<String> parents = new ArrayList<>();
		parents.add((String) parentEntry.get("id"));
		entry.append("graph", graphId).append("parent", parents);
		this.faultyLinks.insert(entry);
		return entry;
	}

	private BasicDBObject addBinaryNode(final AbstractBinaryNode node,
			final String graphId, final AtomicInteger binaryCounter) {
		if (node == null)
			return null;
		final BasicDBObject entry = new BasicDBObject("url", node.getUrl());
		new ArrayList<>();
		entry.append("graph", graphId).append("id",
				graphId + "BN-" + binaryCounter.getAndIncrement());
		final BasicDBList parents = new BasicDBList();
		for (final String parent : node.getParents()) {
			final DBObject parentEntry = this.getHtmlNodeEntry(parent, graphId);
			if (parentEntry != null)
				parents.add(parentEntry.get("id"));
		}
		entry.append("type", node.getContentType())
				.append("label", node.getLabel()).append("parent", parents)
				.append("size", node.getSize());
		this.binaryNodes.insert(entry);
		return entry;
	}

	private BasicDBObject addNode(final HtmlNode node, final String graphId,
			final AtomicInteger htmlNodeCounter) {
		if (node == null)
			return null;
		final BasicDBObject entry = new BasicDBObject("url", node.getUrl());
		entry.append("graph", graphId)
				.append("id",
						graphId + "N-" + htmlNodeCounter.getAndIncrement())
				.append("type", node.getContentType())
				.append("label", node.getLabel())
				.append("internalLinks", node.getInternalEdgesCount())
				.append("externalLinks", node.getExternalEdgesCount())
				.append("faultyLinks", node.getFaultyLinksCount())
				.append("binaryNodes", node.getBinaryNodesCount());
		this.htmlNodes.insert(entry);
		return entry;
	}

	private DBObject getNodeEntry(final AbstractNode node, final String graphId) {
		final BasicDBObject query = new BasicDBObject("url", node.getUrl())
				.append("graph", graphId);
		if (node instanceof HtmlNode)
			return this.htmlNodes.findOne(query);
		else
			return this.binaryNodes.findOne(query);
	}

	private DBObject getHtmlNodeEntry(final String node, final String graphId) {
		final BasicDBObject query = new BasicDBObject("url", node).append(
				"graph", graphId);
		return this.htmlNodes.findOne(query);
	}

	private BasicDBObject addEdge(final Edge edge, final String graphId,
			final AtomicInteger edgesCounter) {
		if (edge == null || graphId == null)
			return null;
		final DBObject startingNodeEntry = this.getNodeEntry(
				edge.getStartingNode(), graphId);
		final DBObject endingNodeEntry = this.getNodeEntry(
				edge.getEndingNode(), graphId);
		if (startingNodeEntry == null || endingNodeEntry == null)
			return null;
		final BasicDBObject entry = new BasicDBObject("source",
				startingNodeEntry.get("id"))
				.append("target", endingNodeEntry.get("id"))
				.append("graph", graphId)
				.append("id", graphId + "E-" + edgesCounter.getAndIncrement())
				.append("value", edge.getStrength());
		this.edges.insert(entry);
		return entry;
	}

	public static DbHandler getInstance() {
		if (instance == null)
			instance = new DbHandler();
		return instance;
	}

}
