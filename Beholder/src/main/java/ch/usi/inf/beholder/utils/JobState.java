package ch.usi.inf.beholder.utils;

public enum JobState {

	ACCEPTED, ERROR, DUPLICATE

}
