package ch.usi.inf.beholder.graph;


public class GenericBinaryNode extends AbstractBinaryNode {

	public GenericBinaryNode(final String url) {
		super(url);
	}

	@Override
	public String getLabel() {
		return this.url.substring(this.url.lastIndexOf('/') + 1);
	}

	@Override
	public String getUrl() {
		return this.url;
	}

	@Override
	public int getColor() {
		return 200;
	}

	@Override
	public String getContentType() {
		return "";
	}

}
