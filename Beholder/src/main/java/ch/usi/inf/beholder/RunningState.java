package ch.usi.inf.beholder;

public class RunningState extends AbstractState {

	private int foundLinks;
	private int completedLinks;
	private int faultyLinks;
	private final String url;

	public RunningState(final String url) {
		this.url = url;
		this.foundLinks = 0;
		this.completedLinks = 0;
		this.faultyLinks = 0;
	}

	public void updateStatus(final int foundLinks, final int completedLinks,
			final int faultyLinks) {
		this.foundLinks = foundLinks;
		this.completedLinks = completedLinks;
		this.faultyLinks = faultyLinks;
	}

	@Override
	public String getState() {
		return "{\"status\" : 0, \"html\" : \"<table> <tbody> <tr> <td><label>Url</label> "
				+ this.url
				+ "</td><td><label>Links Found</label> "
				+ this.foundLinks
				+ "</td></tr><tr><td><label>Pages Completed</label> "
				+ this.completedLinks
				+ "</td><td><label>Faulty Links</label> "
				+ this.faultyLinks + "</td></tr></tbody></table>\"}";
	}

	@Override
	public States getType() {
		return States.RUNNING;
	}

	@Override
	public String getUrl() {
		return this.url;
	}

	public void increaseFoundLinks() {
		this.foundLinks++;
	}

	public void increaseCompletedLinks() {
		this.completedLinks++;
	}

	public void increaseFaultyLinks() {
		this.faultyLinks++;
	}
}
