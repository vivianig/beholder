package ch.usi.inf.beholder;

public class ErrorState extends AbstractState {

	private final String url;

	public ErrorState(final String url) {
		this.url = url;

	}

	@Override
	public States getType() {
		return States.ERROR;
	}

	@Override
	public String getState() {
		return "{\"status\" : 2, \"html\" : \"<div class='crawler_message alert alert-danger fade in'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button><strong>Error:</strong> The crawler was unable to fetch the URL: "
				+ this.url + "</div>\"}";
	}

	@Override
	public String getUrl() {
		return this.url;
	}

}
