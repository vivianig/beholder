package ch.usi.inf.beholder.graph;

public abstract class AbstractNode {

	protected String url;

	protected AbstractNode(final String url) {
		this.url = url;
	}

	public abstract String getLabel();

	public abstract String getUrl();

	@Deprecated
	public abstract int getColor();

	public abstract String getContentType();

	@Override
	public int hashCode() {
		return this.url.hashCode();
	}

}
