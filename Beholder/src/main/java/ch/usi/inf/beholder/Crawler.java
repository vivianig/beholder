package ch.usi.inf.beholder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.ws.http.HTTPException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ch.usi.inf.beholder.graph.AbstractBinaryNode;
import ch.usi.inf.beholder.graph.AbstractNode;
import ch.usi.inf.beholder.graph.Edge;
import ch.usi.inf.beholder.graph.ExternalNode;
import ch.usi.inf.beholder.graph.FaultyLink;
import ch.usi.inf.beholder.graph.GenericBinaryNode;
import ch.usi.inf.beholder.graph.HtmlNode;
import ch.usi.inf.beholder.utils.JobState;

import com.mongodb.BasicDBObject;

public class Crawler {

	private final Set<String> visited;
	private final Set<String> scheduled;

	private final Map<String, List<AbstractNode>> missingEdges;

	private final Map<String, HtmlNode> nodes;
	private final Map<String, AbstractBinaryNode> binaryNodes;
	private final Map<String, AbstractNode> externalNodes;
	private final Map<Edge, Edge> edges;
	private final Map<Edge, Edge> externalEdges;
	private final Map<String, FaultyLink> faultyLinks;

	private String startingUrl;
	private String user;
	private AbstractState state;
	private final Object monitor;

	private final DbHandler dbHandler;
	private ThreadPoolExecutor executor;
	private final List<Future> tasks;
	private final BlockingQueue<Runnable> queue;
	private final AtomicInteger jobStarted;
	private final AtomicInteger jobDone;
	private final AtomicInteger linksFound;

	private final Proxy proxy;

	public Crawler() {
		this.visited = Collections.synchronizedSet(new HashSet<String>());
		this.scheduled = Collections.synchronizedSet(new HashSet<String>());

		this.missingEdges = Collections
				.synchronizedMap(new HashMap<String, List<AbstractNode>>());

		this.nodes = Collections
				.synchronizedMap(new HashMap<String, HtmlNode>());
		this.binaryNodes = Collections
				.synchronizedMap(new HashMap<String, AbstractBinaryNode>());
		this.externalNodes = Collections
				.synchronizedMap(new HashMap<String, AbstractNode>());
		this.edges = Collections.synchronizedMap(new HashMap<Edge, Edge>());
		this.externalEdges = Collections
				.synchronizedMap(new HashMap<Edge, Edge>());
		this.faultyLinks = Collections
				.synchronizedMap(new HashMap<String, FaultyLink>());
		this.dbHandler = DbHandler.getInstance();
		this.tasks = Collections.synchronizedList(new ArrayList<Future>());
		this.queue = new LinkedBlockingQueue<>();
		this.executor = this.createNewExecutor();
		this.jobStarted = new AtomicInteger();
		this.jobDone = new AtomicInteger();
		this.linksFound = new AtomicInteger(1);
		this.monitor = new Object();

		final InetSocketAddress proxyAddr = new InetSocketAddress("localhost",
				9050);
		this.proxy = new Proxy(Proxy.Type.SOCKS, proxyAddr);
	}

	public AbstractState getCrawlerState() {
		return this.state;
	}

	public void stopCrawling() {
		System.out.println("stopping");
		this.executor.shutdownNow();
		for (final Future future : this.tasks) {
			future.cancel(true);
		}
		System.out.println(this.executor.getActiveCount());
	}

	private ThreadPoolExecutor createNewExecutor() {
		final ThreadPoolExecutor executor = new CustomThreadPoolExecutor(30,
				40, 2, TimeUnit.MINUTES, this.queue);
		executor.setRejectedExecutionHandler(new RejectedExecutionHandler() {

			@Override
			public void rejectedExecution(final Runnable r,
					final ThreadPoolExecutor executor) {
				if (executor.isShutdown())
					return;
				System.out.println(executor.isShutdown());
				System.out.println(executor.isTerminating());
				System.out.println("Task Rejected : "
						+ ((CrawlJob) r).getName());
				try {
					Thread.sleep(1000);
				} catch (final InterruptedException e) {
					e.printStackTrace();
				}
				System.out.println("Try again: " + ((CrawlJob) r).getName());
				Crawler.this.tasks.add(executor.submit(r));
				// executor.execute(r);
			}

		});
		return executor;
	}

	public JobState crawl(final String currentUrl, final String currentUser,
			final AbstractState state) throws IllegalArgumentException,
			IOException {
		System.out.println(this.executor.isShutdown());
		System.out.println(this.executor.isTerminating());
		this.executor.prestartAllCoreThreads();
		this.visited.clear();
		this.nodes.clear();
		this.binaryNodes.clear();
		this.externalNodes.clear();
		this.edges.clear();
		this.externalEdges.clear();
		this.faultyLinks.clear();
		this.jobStarted.set(0);
		this.jobDone.set(0);
		this.linksFound.set(1);

		this.startingUrl = currentUrl;
		this.startingUrl = this.startingUrl.split("\\?")[0];
		this.startingUrl = this.startingUrl.split("\\#")[0];
		if (!this.startingUrl.endsWith("/")) {
			this.startingUrl += "/";
		}
		this.user = currentUser;
		this.state = state;

		// check initial url
		final HttpURLConnection connection = (HttpURLConnection) new URL(
				this.startingUrl).openConnection(this.proxy);
		connection.setRequestMethod("HEAD");
		final int code = connection.getResponseCode();
		System.out.println(code);
		connection.disconnect();
		if (code > 400) {
			return JobState.ERROR;
		}

		System.out.println("starting job");
		// this.executor.execute(new CrawlJob(this.startingUrl, null));
		this.tasks.add(this.executor
				.submit(new CrawlJob(this.startingUrl, null)));
		try {
			this.executor.awaitTermination(2, TimeUnit.HOURS);
			this.queue.clear();
			this.executor = this.createNewExecutor();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
		return JobState.ACCEPTED;
	}

	private String getContent(final HttpURLConnection connection)
			throws IOException {
		final BufferedReader rd = new BufferedReader(new InputStreamReader(
				connection.getInputStream()));
		final StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line + '\n');
		}
		return sb.toString();

	}

	private void crawlExternalUrl(final String url, final AbstractNode parent) {
		// TODO implement
	}

	private void crawlInternalUrl(final String url, final AbstractNode parent) {
		// TODO implement
	}

	private void crawl(final String url, final AbstractNode parent) {
		System.out.println(" " + url);
		AbstractNode node = null;

		// TODO this never happens, we add '/' to the url
		if (url.equals("")) {
			System.err.print("Empty url");
			((RunningState) this.state).increaseCompletedLinks();
			throw new IllegalArgumentException();
		}

		// refuse mailto
		if (url.startsWith("mailto:")) {
			System.err.print("Mailto discarded");
			((RunningState) this.state).increaseCompletedLinks();
			return;
		}

		if (this.visited.contains(url)) {
			System.err.println("THIS SHOULD NEVER HAPPEN " + url);
			((RunningState) this.state).increaseCompletedLinks();
			return;
		}
		HttpURLConnection connection = null;
		try {
			// stops crawling on node outside domain
			if (!url.split("#")[0].startsWith(this.startingUrl)
					&& !(url + "/").equals(this.startingUrl)) {
				System.out.println("external " + url);
				// create connection and connect
				connection = (HttpURLConnection) new URL(url)
						.openConnection(this.proxy);
				connection.setRequestMethod("HEAD");
				if (connection.getResponseCode() > 400)
					throw new HTTPException(connection.getResponseCode());

				System.err.println(connection.getResponseCode() + " " + url);
				node = new ExternalNode(url);
				this.externalNodes.put(url, node);
				synchronized (this.monitor) {
					this.visited.add(url);
					this.scheduled.remove(url);
				}
				if (parent != null) {
					this.addEdge(parent, node);
				}
				if (this.missingEdges.get(url) != null) {
					for (final AbstractNode missingParent : this.missingEdges
							.get(url)) {
						this.addEdge(missingParent, node);
					}
				}
				return;

			}
			// create connection and connect
			connection = (HttpURLConnection) new URL(url)
					.openConnection(this.proxy);
			if (connection.getResponseCode() > 400)
				throw new HTTPException(connection.getResponseCode());

			System.err.println(connection.getResponseCode() + " " + url);
			// create node by checking contentType
			String contentType = connection.getContentType();
			if (contentType != null)
				contentType = contentType.split(";")[0];
			node = this.createNode(url, contentType);

			// add node and edges
			if (node instanceof HtmlNode) {
				this.nodes.put(url, (HtmlNode) node);
			} else {
				((AbstractBinaryNode) node).setSize(connection
						.getContentLengthLong());
				this.binaryNodes.put(url, (AbstractBinaryNode) node);
			}

			if (parent != null) {
				this.addEdge(parent, node);
			}
			if (this.missingEdges.get(url) != null) {
				for (final AbstractNode missingParent : this.missingEdges
						.get(url)) {
					this.addEdge(missingParent, node);
				}
			}

			Element body = null;
			Elements urlLinks = null;
			// no need to look for links if it's a binary file
			if ((node instanceof HtmlNode)) {
				// get html content and body
				final Document doc = Jsoup.parse(this.getContent(connection));
				doc.setBaseUri(this.startingUrl);
				((HtmlNode) node).setDoc(doc);
				body = doc.body();

				// set title if it exist
				final String title = doc.title();
				if (title != "")
					((HtmlNode) node).setLabel(title);
				// find links
				if (body != null)
					urlLinks = body.select("a[href]");
			}

			// schedule new thread and add missing edges
			synchronized (this.monitor) {
				this.visited.add(url);
				this.scheduled.remove(url);
				if (this.missingEdges.get(url) != null) {
					for (final AbstractNode missingParent : this.missingEdges
							.get(url)) {
						this.addEdge(missingParent, node);
					}
				}
				if (body == null)
					return;
				for (final Element link : urlLinks) {
					final String newLink = this.purifyUrl(link);
					((RunningState) this.state).increaseFoundLinks();
					if (this.visited.contains(newLink)) {
						this.addEdge(
								node,
								this.nodes.get(newLink) != null ? this.nodes
										.get(newLink)
										: this.externalNodes.get(newLink) != null ? this.externalNodes
												.get(newLink)
												: this.binaryNodes.get(newLink));
						((RunningState) this.state).increaseCompletedLinks();
					} else {
						if (this.missingEdges.get(newLink) == null)
							this.missingEdges
									.put(newLink,
											Collections
													.synchronizedList(new ArrayList<AbstractNode>()));
						this.missingEdges.get(newLink).add(node);
						if (this.scheduled.contains(newLink)) {
							((RunningState) this.state)
									.increaseCompletedLinks();
							continue;
						} else {
							this.scheduled.add(newLink);
							this.tasks.add(this.executor.submit(new CrawlJob(
									newLink, node)));
							// this.executor.execute(new CrawlJob(newLink,
							// node));
							this.linksFound.incrementAndGet();
						}
					}
				}
			}

		} catch (final IOException | HTTPException e) {
			System.err.println("Error: " + e.getMessage() + " " + e.getCause());
			System.out.println(this.nodes.get(e.getMessage()));
			if (parent != null) {
				final Element parentBody = ((HtmlNode) parent).getDoc().body();
				String code = "";
				for (final Element element : parentBody.select("a[href]")) {
					if (this.purifyUrl(element).equals(url)) {
						code = element.toString();
					}
				}
				final FaultyLink faulty = new FaultyLink(url, parent, code);
				this.faultyLinks.put(url, faulty);
				((HtmlNode) parent).increaseFaultyLinksCount();
			}
			((RunningState) this.state).increaseFaultyLinks();
			return;
		} finally {
			if (connection != null)
				connection.disconnect();
			((RunningState) this.state).increaseCompletedLinks();
		}
	}

	/*
	 * private void addEdge(final AbstractNode parent, final AbstractNode node)
	 * { if (parent.getUrl() == node.getUrl()) { // TODO need to handle it
	 * return; } final Edge edge = new Edge(parent, node); if
	 * (this.edges.containsKey(edge)) { this.edges.get(edge).increaseStrength();
	 * } else if (this.externalEdges.containsKey(edge)) {
	 * this.externalEdges.get(edge).increaseStrength(); } else { if (node
	 * instanceof HtmlNode) { ((HtmlNode) parent).increaseInternalEdgesCount();
	 * } else if (node instanceof ExternalNode) { ((HtmlNode)
	 * parent).increaseExternalEdgesCount(); } else { ((AbstractBinaryNode)
	 * node).addParent(parent.getUrl()); ((HtmlNode)
	 * parent).increaseBinaryNodesCount(); } if
	 * (this.nodes.containsKey(node.getUrl()) ||
	 * this.binaryNodes.containsKey(node.getUrl())) {
	 * this.edges.get(edge).increaseStrength(); } else {
	 * 
	 * this.externalEdges.get(edge).increaseStrength(); } } }
	 */
	private void addEdge(final AbstractNode parent, final AbstractNode node) {
		if (parent.getUrl() == node.getUrl()) {
			// TODO need to handle it
			return;
		}
		final Edge edge = new Edge(parent, node);
		if (!(this.edges.containsKey(edge) || this.externalEdges
				.containsKey(edge))) {
			if (node instanceof HtmlNode) {
				((HtmlNode) parent).increaseInternalEdgesCount();
			} else if (node instanceof ExternalNode) {
				((HtmlNode) parent).increaseExternalEdgesCount();
			} else {
				((AbstractBinaryNode) node).addParent(parent.getUrl());
				((HtmlNode) parent).increaseBinaryNodesCount();
			}
		}
		// TODO refactor for clarity?
		if (this.nodes.containsKey(node.getUrl())
				|| this.binaryNodes.containsKey(node.getUrl())) {
			if (this.edges.get(edge) == null)
				this.edges.put(edge, edge);
			else
				this.edges.get(edge).increaseStrength();
		} else {
			if (this.externalEdges.get(edge) == null)
				this.externalEdges.put(edge, edge);
			else
				this.externalEdges.get(edge).increaseStrength();
		}
	}

	public BasicDBObject storeGraph() {
		System.out.println(this.nodes.size());
		System.out.println(this.edges.size());
		System.out.println(this.externalNodes.size());
		System.out.println(this.externalEdges.size());
		return this.dbHandler.addGraph(this.user, this.startingUrl,
				new ArrayList<HtmlNode>(this.nodes.values()),
				new ArrayList<AbstractBinaryNode>(this.binaryNodes.values()),
				new ArrayList<Edge>(this.edges.values()),
				new ArrayList<AbstractNode>(this.externalNodes.values()),
				new ArrayList<Edge>(this.externalEdges.values()),
				new ArrayList<FaultyLink>(this.faultyLinks.values()));
	}

	public String purifyUrl(final Element link) {
		String newLink = link.attr("href");
		if (newLink.equals(null))
			return null;
		if (newLink.startsWith("www.")) {
			newLink = "http://" + newLink;
		} else if (!(newLink.startsWith("http://") || newLink
				.startsWith("https://"))) {
			newLink = link.absUrl("href");
		}
		newLink = newLink.split("\\?")[0];
		newLink = newLink.split("\\#")[0];
		if (newLink.equals(this.startingUrl + "index.html")
				|| newLink.equals(this.startingUrl + "/index.html")
				|| (newLink + "/").equals(this.startingUrl))
			newLink = this.startingUrl;
		return newLink;
	}

	public AbstractNode createNode(final String url, final String type) {
		if (url == null)
			return null;
		if (type == null)
			return new GenericBinaryNode(url);
		AbstractNode node = null;
		switch (type) {
			case "text/html":
				node = new HtmlNode(url);
				break;
			default:
				node = new GenericBinaryNode(url);
				break;
		}
		return node;
	}

	public HashSet<AbstractNode> getNodes() {
		return new HashSet<AbstractNode>(this.nodes.values());
	}

	public HashSet<Edge> getEdges() {
		return new HashSet<Edge>(this.edges.values());
	}

	private class CrawlJob implements Runnable {
		private final String url;
		private final AbstractNode parent;

		public CrawlJob(final String url, final AbstractNode parent) {
			this.url = url;
			this.parent = parent;
		}

		@Override
		public void run() {
			Crawler.this.crawl(this.url, this.parent);
		}

		public String getName() {
			return this.url;
		}
	}

	private class CustomThreadPoolExecutor extends ThreadPoolExecutor {
		public CustomThreadPoolExecutor(final int corePoolSize,
				final int maximumPoolSize, final long keepAliveTime,
				final TimeUnit unit, final BlockingQueue<Runnable> workQueue) {
			super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
		}

		@Override
		protected void beforeExecute(final Thread t, final Runnable r) {
			super.beforeExecute(t, r);
			Crawler.this.jobStarted.incrementAndGet();

		}

		@Override
		protected void afterExecute(final Runnable r, final Throwable t) {
			super.afterExecute(r, t);
			Crawler.this.jobDone.incrementAndGet();
			// if (Crawler.this.jobDone.get() == Crawler.this.linksFound.get())
			// {
			if (Crawler.this.executor.getActiveCount() == 1) {
				System.out.println("shutting down with: "
						+ Crawler.this.jobDone + " " + Crawler.this.linksFound
						+ " " + Crawler.this.jobStarted + " "
						// + ((CrawlJob) r).getName() + " "
						+ r.toString() + " "
						+ Crawler.this.executor.getActiveCount());
				Crawler.this.jobDone.set(0);
				Crawler.this.linksFound.set(1);
				Crawler.this.jobStarted.set(0);
				Crawler.this.executor.shutdown();
			} else {
				System.out.println(Crawler.this.jobDone + " "
						+ Crawler.this.linksFound + " "
						+ Crawler.this.jobStarted + " "
						// + ((CrawlJob) r).getName() + " "
						+ r.toString() + " "
						+ Crawler.this.executor.getActiveCount());
			}
		}
	}
}
