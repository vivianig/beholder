package ch.usi.inf.beholder.graph;

import java.util.ArrayList;

public abstract class AbstractBinaryNode extends AbstractNode {

	protected ArrayList<String> parents;
	protected long size;

	protected AbstractBinaryNode(final String url) {
		super(url);
		this.parents = new ArrayList<>();
	}

	public ArrayList<String> getParents() {
		return this.parents;
	}

	public void addParent(final String url) {
		this.parents.add(url);
	}

	public long getSize() {
		return this.size;
	}

	public void setSize(final long size) {
		this.size = size;

	}

}
