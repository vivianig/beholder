package ch.usi.inf.beholder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;

import ch.usi.inf.beholder.graph.AbstractNode;
import ch.usi.inf.beholder.graph.Edge;

@Deprecated
public class GraphCreator {

	@Deprecated
	public static void generateGraph(final HashSet<AbstractNode> nodes,
			final HashSet<Edge> edges) {
		try {
			final File file = new File("graph.vna");
			file.delete();
			file.createNewFile();

			final FileWriter fw = new FileWriter(file.getAbsoluteFile());
			final BufferedWriter bw = new BufferedWriter(fw);

			bw.write("*node data\n");
			bw.write("ID url\n");
			for (final AbstractNode node : nodes) {
				bw.write(node.getUrl() + " " + node.getUrl() + "\n");
			}
			bw.write("*node properties\n");
			bw.write("ID size color shortlabel\n");
			for (final AbstractNode node : nodes) {
				bw.write(node.getUrl() + " 1 " + " " + node.getColor() + " "
						+ node.getLabel() + "\n");
			}

			bw.write("*tie data\n");
			bw.write("from to strength\n");
			for (final Edge edge : edges) {
				bw.write(edge.getStartingNode().getUrl() + " "
						+ edge.getEndingNode().getUrl() + " 1\n");
			}
			bw.close();

		} catch (final IOException e) {
			e.printStackTrace();

		}
	}
}
